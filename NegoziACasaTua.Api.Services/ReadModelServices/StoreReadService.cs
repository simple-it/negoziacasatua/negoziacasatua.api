using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Factories;
using NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Services.ReadModelServices
{
    public class StoreReadService : IStoreReadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _settings;

        public StoreReadService(IUnitOfWork unitOfWork, IOptions<AppSettings> settings)
        {
            _unitOfWork = unitOfWork;
            _settings = settings.Value;
        }
        
        public async Task<InformationTypeJson> GetAllInformationTypes()
        {
            var categoriesDto = await _unitOfWork.CategoryRepository.GetQueryable().ToListAsync();
            var categories = categoriesDto.Select(t => new TypeJson
            {
                Id = t.Id,
                Name = t.Name
            });
            var contactTypesDto = await _unitOfWork.ContactTypeRepository.GetQueryable().ToListAsync();
            var contactTypes = contactTypesDto.Select(t => new TypeJson
            {
                Id = t.Id,
                Name = t.Name
            });
            var paymentTypesDto = await _unitOfWork.PaymentTypeRepository.GetQueryable().ToListAsync();
            var paymentTypes = paymentTypesDto.Select(t => new TypeJson
            {
                Id = t.Id,
                Name = t.Name
            });
            
            var info = new InformationTypeJson
            {
                Categories = categories,
                ContactTypes = contactTypes,
                PaymentTypes = paymentTypes
            };
            return info;
        }

        public async Task<IEnumerable<StoreAdminJson>> GetAllStores(bool validated = true)
        {
            var stores = await _unitOfWork.StoreRepository.GetQueryable()
                .Include(t => t.Owner).OrderBy(t => t.Name)
                .Where(t => t.Validated == validated && t.DeletedDate == null).ToListAsync();

            return stores.Select(t => t.ToAdminJson());
        }

        public async Task<ValueJson> GetStoresToValidateCount()
        {
            var storesCount = await _unitOfWork.StoreRepository.GetQueryable()
                .CountAsync(t => !t.Validated && t.DeletedDate == null);
            return new ValueJson
            {
                Value = storesCount
            };
        }

        public async Task<CategoryWithStoreJson> GetStoresByCategoryId(int categoryId)
        {
            var categoryDto = await _unitOfWork.CategoryRepository.GetQueryable()
                .Include(t => t.Stores)
                .FirstOrDefaultAsync(t => t.Id == categoryId);
            if(categoryDto == null)
                throw new Exception("Nessuna categoria trovata!");
            return new CategoryWithStoreJson
            {
                Name = categoryDto.Name,
                Stores = categoryDto.Stores.Where(t => t.Validated && t.DeletedDate == null)
                    .OrderBy(t => t.Name)
                    .Select(t => t.ToLiteJson())
            };
        }

        public async Task<StoreJson> GetStoreById(int id, bool isAdmin)
        {
            var store = await _unitOfWork.StoreRepository.GetQueryable().Include(t => t.Images)
                .Include(t => t.Category)
                .Include(t => t.Contacts).ThenInclude(t => t.ContactType)
                .Include(t => t.Payments).ThenInclude(t => t.PaymentType)
                .FirstOrDefaultAsync(t => t.Id == id);
            
            if (store == null)
                throw new Exception("Negozio non trovato");
            if (store.DeletedDate != null)
                throw new Exception("Il negozio è stato cancellato!");
            if (!store.Validated && !isAdmin)
                throw new Exception("Il negozio non è stato ancora validato e non hai i permessi per vederlo.");

            return store.ToJson(_settings.Storage.StorageBasePath);
        }

        public async Task<int> GetStoreIdByOwnerId(int ownerId)
        {
            var userDto = await _unitOfWork.AccountRepository.GetQueryable().Include(t => t.Store)
                .FirstOrDefaultAsync(t => t.Id == ownerId);

            return userDto.Store.Id;
        }

        public async Task<StoreInfoJson> GetStoreInfo(int id)
        {
            var store = await _unitOfWork.StoreRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == id);
            
            return new StoreInfoJson
            {
                Validated = store.Validated
            };
        }

        public async Task<IEnumerable<StoreLiteJson>> SearchStore(string search, int resultsLimit, int page)
        {
            var searchLower = search.ToLower();
            var stores = await _unitOfWork.StoreRepository.GetQueryable()
                .Where(t =>
                    t.Validated &&
                    t.Name.ToLower().Contains(searchLower) || t.Description.ToLower().Contains(searchLower)
                         ).Skip(resultsLimit * page).Take(resultsLimit).ToListAsync();

            var storesJson = stores.Where(t => t.DeletedDate == null)
                .Select(t => t.ToLiteJson());
            return storesJson;
        }
    }
}