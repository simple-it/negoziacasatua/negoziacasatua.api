using System.Threading.Tasks;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface IStorageService
    {
        Task UploadFile(string path, string base64);
        Task DeleteFile(string path);
        void BeginTransaction();
        Task CommitAsync();
        Task RollbackAsync();
    }
}