using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Google;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using NegoziACasaTua.Api.Services.Utilities;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace NegoziACasaTua.Api.Services.Services
{
    public class GoogleStorageService : IStorageService
    {
        private readonly AppSettings _settings;
        private readonly StorageClient _client;
        private readonly ILogger<GoogleStorageService> _logger;

        private bool IsInTransaction = false;
        private List<Task> _transactionUploads;
        private List<string> _fileUploaded;
        private List<string> _fileToDelete;
        private CancellationTokenSource _transactionCancellation;

        public GoogleStorageService(IOptions<AppSettings> settings, ILogger<GoogleStorageService> logger)
        {
            _logger = logger;
            _settings = settings.Value;
            
            var credentialJson = Environment.GetEnvironmentVariable(_settings.Storage.CredentialJsonVariableName);
            _client = StorageClient.Create(GoogleCredential.FromJson(credentialJson));
        }

        private void SuppressTransactionVariables()
        {
            GC.SuppressFinalize(_transactionUploads);
            GC.SuppressFinalize(_fileUploaded);
            GC.SuppressFinalize(_transactionCancellation);
            
            _transactionUploads = null;
            _fileUploaded = null;
            _transactionCancellation = null;
        }

        private async Task UploadFileInTransaction(string path, Stream stream)
        {
            try
            {
                await _client.UploadObjectAsync(_settings.Storage.BucketName, path, null, stream,
                    cancellationToken: _transactionCancellation.Token);
                _fileUploaded.Add(path);
            }
            catch (TaskCanceledException e)
            {
                _logger.LogWarning("[GoogleStorageService] - Upload file task canceled in transaction");
                _logger.LogWarning("[GoogleStorageService] - " + e.Message);
            }
        }
        

        public async Task UploadFile(string path, string base64)
        {
            try
            {
                var bytes = Convert.FromBase64String(base64);
                var stream = new MemoryStream(bytes);
                if (IsInTransaction)
                {
                    _transactionUploads.Add(UploadFileInTransaction(path, stream));
                    return;
                }
            
                await _client.UploadObjectAsync(_settings.Storage.BucketName, path, null, stream);
            }
            catch (Exception e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw new Exception("Errore di caricamento di un'immagine");
            }
        }

        public async Task DeleteFile(string path)
        {
            if (IsInTransaction)
            {
                _fileToDelete.Add(path);
                return;
            }

            try
            {
                await _client.DeleteObjectAsync(_settings.Storage.BucketName, path);
            }
            catch (GoogleApiException e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
            }
        }

        public void BeginTransaction()
        {
            IsInTransaction = true;
            _transactionUploads = new List<Task>();
            _fileUploaded = new List<string>();
            _fileToDelete = new List<string>();
            _transactionCancellation = new CancellationTokenSource();
            
        }

        public async Task CommitAsync()
        {
            IsInTransaction = false;
            try
            {
                var tasks = new List<Task>();
                foreach (var filePath in _fileToDelete)
                {
                    tasks.Add(DeleteFile(filePath));
                }

                await Task.Run(() =>
                {
                    Task.WaitAll(tasks.ToArray());
                    Task.WaitAll(_transactionUploads.ToArray());
                });

                SuppressTransactionVariables();
            }
            catch (Exception e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw new Exception("Errore durante il salvataggio delle immagini.");
            }
        }

        public async Task RollbackAsync()
        {
            IsInTransaction = false;
            var tasks = new List<Task>();
            _transactionCancellation.Cancel();
            await Task.Run(() =>
            {
                Task.WaitAll(_transactionUploads.ToArray());
            });
            foreach (var filePath in _fileUploaded)
            {
                tasks.Add(DeleteFile(filePath));
            }
            foreach (var task in tasks)
            {
                await task;
            }
            SuppressTransactionVariables();
        }
    }
}