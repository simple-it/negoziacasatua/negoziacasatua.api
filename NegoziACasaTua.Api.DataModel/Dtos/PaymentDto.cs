using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class PaymentDto : DtoRoot
    {
        public StoreDto Store { get; set; }
        public PaymentTypeDto PaymentType { get; set; }
    }
}