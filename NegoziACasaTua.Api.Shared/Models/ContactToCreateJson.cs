namespace NegoziACasaTua.Api.Shared.Models
{
    public class ContactToCreateJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ContactTypeId { get; set; }
    }
}