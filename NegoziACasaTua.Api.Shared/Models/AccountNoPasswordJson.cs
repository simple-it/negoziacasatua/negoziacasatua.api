namespace NegoziACasaTua.Api.Shared.Models
{
    public class AccountNoPasswordJson
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}