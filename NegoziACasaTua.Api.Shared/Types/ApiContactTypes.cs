namespace NegoziACasaTua.Api.Shared.Types
{
    public class ApiContactTypes
    {
        public const int Email = 1;
        public const int WhatsApp = 2;
        public const int Telephone = 3;
    }
}