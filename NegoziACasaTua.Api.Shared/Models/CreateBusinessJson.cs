namespace NegoziACasaTua.Api.Shared.Models
{
    public class CreateBusinessJson
    {
        public AccountCredentialsJson Account { get; set; }
        public StoreToCreateJson Store { get; set; }
    }
}