namespace NegoziACasaTua.Api.Shared.Models.v2
{
    public class PasswordJson
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}