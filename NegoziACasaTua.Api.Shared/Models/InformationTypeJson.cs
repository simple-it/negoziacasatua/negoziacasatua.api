using System.Collections.Generic;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class InformationTypeJson
    {
        public IEnumerable<TypeJson> ContactTypes { get; set; }
        public IEnumerable<TypeJson> Categories { get; set; }
        public IEnumerable<TypeJson> PaymentTypes { get; set; }
    }
}