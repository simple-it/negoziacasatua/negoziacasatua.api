namespace NegoziACasaTua.Api.Shared.Models.v2
{
    public class EmailJson
    {
        public string Email { get; set; }
    }
}