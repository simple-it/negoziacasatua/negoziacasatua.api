using System;
using System.IO;

namespace NegoziACasaTua.Api.Shared.ValueObjects
{
    public class StringValidator : Validator<string>
    {
        public StringValidator(string obj, string message) : base(obj)
        {
            IsNotEmpty(message);
        }

        public StringValidator HasMinLength(int min, string message)
        {
            if(obj.Length < min)
                throw new Exception(message);
            return this;
        }

        public StringValidator IsEmail(string message)
        {
            try {
                var addr = new System.Net.Mail.MailAddress(obj); 
                if(addr.Address != obj)
                    throw new Exception();
            }
            catch {
                throw new Exception(message);
            }
            return this;
        }

        private void IsNotEmpty(string message)
        {
            if(string.IsNullOrEmpty(obj))
                throw new Exception(message);
        }
    }
}