# Use Microsoft's official build .NET image.
# https://hub.docker.com/_/microsoft-dotnet-core-sdk/
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

# Install production dependencies.
# Copy csproj and restore as distinct layers.
WORKDIR /src
COPY *.sln ./
COPY NegoziACasaTua.Api/*.csproj ./NegoziACasaTua.Api/
COPY NegoziACasaTua.Api.Mediator/*.csproj ./NegoziACasaTua.Api.Mediator/
COPY NegoziACasaTua.Api.DataModel/*.csproj ./NegoziACasaTua.Api.DataModel/
COPY NegoziACasaTua.Api.Services/*.csproj ./NegoziACasaTua.Api.Services/
COPY NegoziACasaTua.Api.Shared/*.csproj ./NegoziACasaTua.Api.Shared/

RUN dotnet restore
COPY . ./

# Building each project
WORKDIR /src/NegoziACasaTua.Api.Mediator
RUN dotnet build -c Release -o /app

WORKDIR /src/NegoziACasaTua.Api.DataModel
RUN dotnet build -c Release -o /app

WORKDIR /src/NegoziACasaTua.Api.Services
RUN dotnet build -c Release -o /app

WORKDIR /src/NegoziACasaTua.Api.Shared
RUN dotnet build -c Release -o /app

WORKDIR /src/NegoziACasaTua.Api
RUN dotnet build -c Release -o /app
# Publish
RUN dotnet publish -c Release -o /app

# Use Microsoft's official runtime .NET image.
# https://hub.docker.com/_/microsoft-dotnet-core-aspnet/
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app ./

# Run the web service on container startup.
ENTRYPOINT ["dotnet", "NegoziACasaTua.Api.dll"]
