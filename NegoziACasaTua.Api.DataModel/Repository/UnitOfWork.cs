﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.DataModel.Facade;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;

namespace NegoziACasaTua.Api.DataModel.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataModelFacade _dataModelFacade;
        private readonly ILogger<UnitOfWork> _logger;
        private IDbContextTransaction _transaction;

        public UnitOfWork(DataModelFacade dataModelFacade, ILogger<UnitOfWork> logger)
        {
            this._dataModelFacade = dataModelFacade;
            _logger = logger;
        }

        private IRepository<AccountDto> _accountRepository;
        public IRepository<AccountDto> AccountRepository =>
            _accountRepository ??= new Repository<AccountDto>(_dataModelFacade);

        private IRepository<AccountTypeDto> _accountTypeRepository;
        public IRepository<AccountTypeDto> AccountTypeRepository =>
            _accountTypeRepository ??= new Repository<AccountTypeDto>(_dataModelFacade);
        
        private IRepository<CategoryDto> _categoryRepository;
        public IRepository<CategoryDto> CategoryRepository =>
            _categoryRepository ??= new Repository<CategoryDto>(_dataModelFacade);

        private IRepository<ContactDto> _contactRepository;
        public IRepository<ContactDto> ContactRepository =>
            _contactRepository ??= new Repository<ContactDto>(_dataModelFacade);
        
        private IRepository<ContactTypeDto> _contactTypeRepository;
        public IRepository<ContactTypeDto> ContactTypeRepository =>
            _contactTypeRepository ??= new Repository<ContactTypeDto>(_dataModelFacade);
        
        private IRepository<ImageDto> _imageRepository;
        public IRepository<ImageDto> ImageRepository =>
            _imageRepository ??= new Repository<ImageDto>(_dataModelFacade);

        private IRepository<PaymentDto> _paymentRepository;
        public IRepository<PaymentDto> PaymentRepository =>
            _paymentRepository ??= new Repository<PaymentDto>(_dataModelFacade);

        private IRepository<PaymentTypeDto> _paymentTypeRepository;

        public IRepository<PaymentTypeDto> PaymentTypeRepository =>
            _paymentTypeRepository ??= new Repository<PaymentTypeDto>(_dataModelFacade);

        private IRepository<StoreDto> _storeRepository;
        public IRepository<StoreDto> StoreRepository =>
            _storeRepository ??= new Repository<StoreDto>(_dataModelFacade);


        
        public async Task BeginTransactionAsync()
        {
            _transaction = await _dataModelFacade.Database.BeginTransactionAsync();
        }

        public async Task CommitAsync()
        {
            try
            {
                await _dataModelFacade.SaveChangesAsync();
                await _transaction.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"[UnitOfWork.CommitAsync] - {ex.Message}");
                if (ex.InnerException != null)
                    _logger.LogError($"[UnitOfWork.CommitAsync] - Inner Exception - {ex.InnerException.Message}");
                throw new Exception("Errore durante il salvataggio dei dati.");
            }
        }

        public async Task RollbackAsync()
        {
            if(_transaction != null) 
                await _transaction.RollbackAsync();
        }

        public async Task SaveAsync()
        {
            try
            {
                await this._dataModelFacade.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError($"[UnitOfWork.SaveAsync] - {ex.Message}");
                if (ex.InnerException != null)
                    _logger.LogError($"[UnitOfWork.SaveAsync] - Inner Exception - {ex.InnerException.Message}");
                throw new Exception("Errore durante il salvataggio dei dati.");
            }
        }
    }
}