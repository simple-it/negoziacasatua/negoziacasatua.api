using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Types;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/[controller]")]
    public class StoreController : BaseController
    {
        private readonly ILogger<InitController> _logger;
        private readonly IStoreReadService _storeReadService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="storeReadService"></param>
        public StoreController(ILogger<InitController> logger, IStoreReadService storeReadService) : base(logger)
        {
            this._logger = logger;
            _storeReadService = storeReadService;
        }


        /// <summary>
        /// Get all information to create store
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<InformationTypeJson>> GetAllInformationTypes()
        {
            try
            {
                var informationType = await _storeReadService.GetAllInformationTypes();
                return this.Ok(informationType);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Get all stores by category ordered by name.
        /// </summary>
        /// <returns></returns>
        [HttpGet("bycategory/{categoryId}")]
        public async Task<ActionResult<CategoryWithStoreJson>> GetStoresByCategoryId([FromRoute] int categoryId)
        {
            try
            {
                var stores = await _storeReadService.GetStoresByCategoryId(categoryId);
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Get the store by id
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [HttpGet("{storeId}")]
        public async Task<ActionResult<StoreJson>> GetStoreById([FromRoute] int storeId)
        {
            try
            {
                var isAdmin = User.HasClaim(ApiClaimTypes.StoreId, storeId.ToString()) ||
                              User.IsInRole(ApiAccountTypes.Admin.ToString()) ||
                              User.IsInRole(ApiAccountTypes.Municipality.ToString());
                var store = await _storeReadService.GetStoreById(storeId, isAdmin);
                return this.Ok(store);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Search from all the stores available (pages start from 0)
        /// </summary>
        /// <returns></returns>
        [HttpGet("search/{search}/{resultsLimit}/{page}")]
        public async Task<ActionResult<IEnumerable<StoreLiteJson>>> SearchStore([FromRoute] string search, [FromRoute] int resultsLimit, [FromRoute] int page)
        {
            try
            {
                var stores = await _storeReadService.SearchStore(search, resultsLimit, page);
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}