using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.DataModel.Factories;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NegoziACasaTua.Api.Shared.MailTemplateData;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.Types;

namespace NegoziACasaTua.Api.Services.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _settings;
        private readonly ILogger _logger;
        
        public AccountService(IUnitOfWork unitOfWork, IOptions<AppSettings> settings,
            ILogger<AccountService> logger, IMailService mailService)
        {
            _unitOfWork = unitOfWork;
            _settings = settings.Value;
            _logger = logger;
        }
        
        public async Task<AccountInfoJson> AuthenticateAsync(AccountCredentialsJson accountCredentialsJson)
        {
            if(string.IsNullOrEmpty(accountCredentialsJson.Email))
                throw new Exception("Devi inserire un'email!");
            if(string.IsNullOrEmpty(accountCredentialsJson.Password))
                throw new Exception("Devi inserire una pasword!");
            var account = await _unitOfWork.AccountRepository.GetQueryable().Where(x =>
                    x.Email == accountCredentialsJson.Email &&
                    x.Password == new Password(accountCredentialsJson.Password).GetValue())
                .Include(x => x.AccountType)
                .Include(t => t.Store)
                .FirstOrDefaultAsync();

            if(account == null)
                throw new Exception("Credenziali non valide!");

            var expirationDate = DateTime.Now.AddHours(this._settings.TokenAuthentication.TokenExpirationHours);
            var token = CreateToken(expirationDate, GetClaims(account));

            var accountInfo = new AccountInfoJson()
            {
                Id = account.Id,
                Email = account.Email,
                StoreId = account.Store?.Id ?? 0,
                StoreName = account.Store?.Name,
                AccountTypeId = account.AccountType.Id,
                Token = token,
                TokenExpiration = expirationDate
            };

            return accountInfo;
        }

        private Claim[] GetClaims(AccountDto account)
        {
            return new[]
            {
                new Claim(ClaimTypes.Name, account.Id.ToString()),
                new Claim(ClaimTypes.Role, account.AccountType.Id.ToString()),
                new Claim(ApiClaimTypes.StoreId, account.Store?.Id.ToString() ?? "")
            };
        }

        private string CreateToken(DateTime expirationDate, Claim[] claims)
        {
            var securityKey = Encoding.ASCII.GetBytes(this._settings.TokenAuthentication.SecretKey);
            var jwt = new JwtSecurityToken(
                issuer: this._settings.TokenAuthentication.Issuer,
                audience: this._settings.TokenAuthentication.Audience,
                claims: claims,
                notBefore: DateTime.Now,
                expires: expirationDate,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(securityKey),
                    SecurityAlgorithms.HmacSha256
                ));
            return new JwtSecurityTokenHandler().WriteToken(jwt);;
        }


        public async Task<int> CreateAccountAsync(AccountCredentialsJson userJson, int accountTypeId)
        {
            var accountType = await _unitOfWork.AccountTypeRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == accountTypeId);
            if (accountType == null)
                throw new Exception("Il ruolo inserito non esiste!");
            if (await _unitOfWork.AccountRepository.GetQueryable()
                    .CountAsync(t => t.Email == userJson.Email) != 0)
                throw new Exception("La mail è già stata usata!");

            var user = AccountDtoFactory.FromJson(userJson, accountType);
            await _unitOfWork.AccountRepository.Insert(user);
            await _unitOfWork.SaveAsync();
            return user.Id;
        }

        public async Task UpdateAccountAsync(AccountJson accountJson)
        {
            var oldUser = await _unitOfWork.AccountRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == accountJson.Id);
            if(oldUser == null)
                throw new Exception("Utente non trovato!");
            
            if (accountJson.Password == "")
                accountJson.Password = oldUser.Password;
            
            var accountDto = oldUser.FromJson(accountJson, oldUser.AccountType);

            _unitOfWork.AccountRepository.Update(accountDto);
        }

        public async Task UpdatePasswordAsync(int accountId, PasswordJson passwordJson)
        {
            var account = await _unitOfWork.AccountRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == accountId &&
                                          t.Password == new Password(passwordJson.OldPassword).GetValue());
            if(account == null)
                throw new Exception("La vecchia password non è corretta.");

            account.UpdatePassword(passwordJson.NewPassword);
            _unitOfWork.AccountRepository.Update(account);
        }

        public async Task<IEnumerable<AccountJson>> GetAllAccountsAsync()
        {
            var accountsDto = await _unitOfWork.AccountRepository.GetQueryable()
                .Include(t => t.AccountType).ToListAsync();
            var accountsJson = accountsDto.Select(AccountDtoFactory.ToJson).ToList();
            return accountsJson;
        }

        public async Task<AccountJson> GetAccountByIdAsync(int id)
        {
            var user = await _unitOfWork.AccountRepository.GetQueryable()
                .Include(t => t.AccountType)
                .FirstOrDefaultAsync(x => x.Id == id);
        
            if (user == null) 
                throw new Exception("Utente non trovato");
        
            return user.ToJson();
        }

        public async Task DeleteAccountAsync(int id)
        {
            await _unitOfWork.AccountRepository.Delete(id);
        }

        public async Task<IEnumerable<TypeJson>> GetAllAccountTypes()
        {
            var accountTypesDto = await _unitOfWork.AccountTypeRepository.GetQueryable().ToListAsync();
            return accountTypesDto.Select(t => new TypeJson
            {
                Id = t.Id,
                Name = t.Name
            });
        }

        public async Task<IEnumerable<AccountTypeJson>> GetAllAccountTypesWithAccounts()
        {
            var accountTypes = await _unitOfWork.AccountTypeRepository.GetQueryable()
                .Include(t => t.Accounts).Where(t => t.Id != ApiAccountTypes.Business)
                .ToListAsync();

            var accountTypeJsons = accountTypes.Select(t => 
                t.ToJson(
                    t.Accounts.Where(account => 
                        !string.IsNullOrEmpty(account.Password)).Select(dto => dto.ToNoPasswordJson())

            ));

            return accountTypeJsons;
        }
    }
}