namespace NegoziACasaTua.Api.Shared.Configuration
{
    public class AppSettings
    {
        public string MySqlConnectionString { get; set; }
        public Mail Mail { get; set; }
        public Storage Storage { get; set; }
        public TokenAuthentication TokenAuthentication { get; set; }
    }
}