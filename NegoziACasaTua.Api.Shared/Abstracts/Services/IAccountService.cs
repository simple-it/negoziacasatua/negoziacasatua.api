using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface IAccountService
    {
        Task<AccountInfoJson> AuthenticateAsync(AccountCredentialsJson accountCredentialsJson);

        Task<int> CreateAccountAsync(AccountCredentialsJson userJson, int accountTypeId);
        Task UpdateAccountAsync(AccountJson accountJson);
        Task UpdatePasswordAsync(int accountId, PasswordJson passwordJson);
        Task<IEnumerable<AccountJson>> GetAllAccountsAsync();
        Task<AccountJson> GetAccountByIdAsync(int id);
        Task DeleteAccountAsync(int id);
        Task<IEnumerable<TypeJson>> GetAllAccountTypes();
        Task<IEnumerable<AccountTypeJson>> GetAllAccountTypesWithAccounts();
    }
}