using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.DataModel.Factories
{
    public static class CategoryDtoFactory
    {
        public static CategoryJson ToJson(this CategoryDto dto, string baseImagePath)
        {
            return new CategoryJson
            {
                Id = dto.Id,
                Name = dto.Name,
                ImagePath = baseImagePath + dto.ImagePath
            };
        }
     }
}