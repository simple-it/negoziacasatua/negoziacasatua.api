using NegoziACasaTua.Api.Services.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using Microsoft.Extensions.DependencyInjection;

namespace NegoziACasaTua.Api.Mediator
{
    public static class OrchestratorsHelper
    {
        public static IServiceCollection AddOrchestrators(this IServiceCollection services)
        {
            services.AddTransient<IAccountOrchestrator, AccountOrchestrator>();
            services.AddTransient<IInitOrchestrator, InitOrchestrator>();
            services.AddTransient<IStoreOrchestrator, StoreOrchestrator>();
            services.AddTransient<ICategoryOrchestrator, CategoryOrchestrator>();
            
            return services;
        }
    }
}