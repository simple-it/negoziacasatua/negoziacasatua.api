using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class ContactDto : DtoRoot
    {
        public string Name { get; set; }

        public ContactTypeDto ContactType { get; set; }
        public StoreDto Store { get; set; }
    }
}