namespace NegoziACasaTua.Api.Shared.Configuration
{
    public class TokenAuthentication
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string TokenPath { get; set; }
        public int TokenExpirationHours { get; set; }
        public string CookieName { get; set; }
    }
}