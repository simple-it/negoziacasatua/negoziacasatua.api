using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class AccountTypeDto : DtoRoot
    {
        public string Name { get; set; }
        
        public ICollection<AccountDto> Accounts { get; set; }
    }
}