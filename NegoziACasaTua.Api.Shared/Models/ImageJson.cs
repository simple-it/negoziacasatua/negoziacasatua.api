namespace NegoziACasaTua.Api.Shared.Models
{
    public class ImageJson
    {
        public int Id { get; set; }
        public string Path { get; set; }
    }
}