using System;
using Microsoft.Extensions.Logging;

namespace NegoziACasaTua.Api.Services.Utilities
{
    public static class CommonServices
    {
        public static void LogDefaultExceptionErrors(Exception e, ILogger logger, object thisObj)
        {
            logger.LogError($"[{thisObj.GetType().Name}] - Exception - {e.Message}");
            if (e.InnerException != null)
                logger.LogError($"[{thisObj.GetType().Name}] - InnerException - {e.InnerException.Message}");
        }

        public static void ThrowExIfNull(object obj, string message)
        {
            if(obj == null)
                throw new Exception(message);
        }
    }
}