namespace NegoziACasaTua.Api.Shared.Models
{
    public class CategoryJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
    }
}