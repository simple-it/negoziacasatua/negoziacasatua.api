using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NegoziACasaTua.Api.DataModel.Dtos;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class ContactMapping : IEntityTypeConfiguration<ContactDto>
    {
        public void Configure(EntityTypeBuilder<ContactDto> builder)
        {
            builder.ToTable("Contact");

            builder.HasOne(t => t.Store)
                .WithMany(t => t.Contacts);
            builder.HasOne(t => t.ContactType)
                .WithMany(t => t.Contacts);
            
            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Name)
                .HasMaxLength(64)
                .IsRequired();
        }
    }
}