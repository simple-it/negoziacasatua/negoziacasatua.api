using NegoziACasaTua.Api.Services.Services;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using Microsoft.Extensions.DependencyInjection;

namespace NegoziACasaTua.Api.Mediator
{
    public static class ServicesHelper
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            // Transient = created each time they're requested from the service container
            // Scoped = created once per client request
            // Singleton
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IStoreService, StoreService>();
            services.AddScoped<IImageService, ImageService>();
            services.AddScoped<IStorageService, GoogleStorageService>();
            services.AddScoped<IMailService, SendGridMailService>();

            return services;
        }
    }
}