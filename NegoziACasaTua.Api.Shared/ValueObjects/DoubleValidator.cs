using System;

namespace NegoziACasaTua.Api.Shared.ValueObjects
{
    public class DoubleValidator : Validator<double>
    {
        public DoubleValidator(double obj, string message) : base(obj, message)
        {
        }

        public DoubleValidator IsGreaterThanZero(string message)
        {
            
            return this;
        }
    }
}