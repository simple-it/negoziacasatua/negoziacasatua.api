using System;

namespace NegoziACasaTua.Api.Shared.Models.v2
{
    public class StoreDeletedJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeletingDate { get; set; }
    }
}