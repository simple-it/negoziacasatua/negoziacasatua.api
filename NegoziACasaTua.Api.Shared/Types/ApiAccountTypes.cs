namespace NegoziACasaTua.Api.Shared.Types
{
    public class ApiAccountTypes
    {
        public const int Admin = 1;
        public const int Municipality = 2;
        public const int Business = 3;
    }
}