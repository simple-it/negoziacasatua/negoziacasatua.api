using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.DataModel.Factories
{
    public static class AccountDtoFactory
    {
        private static void Validate(this AccountDto dto)
        {
            new StringValidator(dto.Email, "L'email deve essere inserita!")
                .IsEmail("L'email inserita non è valida!");
            new StringValidator(dto.Password, "La password deve essere inserita!")
                .HasMinLength(6, "La password deve avere almeno 6 caratteri!");
        }

        public static AccountDto UpdatePassword(this AccountDto dto, string password)
        {
            dto.Password = new Password(password).GetValue();
            dto.Validate();
            return dto;
        }

        public static AccountDto FromJson(this AccountDto dto, AccountJson json, AccountTypeDto accountTypeDto)
        {
            dto.Id = json.Id;
            dto.Email = json.Email;
            dto.Password = new Password(json.Password).GetValue();
            dto.AccountType = accountTypeDto;
            
            dto.Validate();
            return dto;
        }

        public static AccountDto FromJson(this AccountDto dto, AccountCredentialsJson json, AccountTypeDto accountTypeDto)
        {
            dto.Email = json.Email;
            dto.Password = new Password(json.Password).GetValue();
            dto.AccountType = accountTypeDto;
            
            dto.Validate();
            return dto;
        }
        
        public static AccountDto FromJson(AccountJson json, AccountTypeDto accountTypeDto)
        {
            var dto = new AccountDto().FromJson(json, accountTypeDto);
            return dto;
        }
        
        public static AccountDto FromJson(AccountCredentialsJson json, AccountTypeDto accountTypeDto)
        {
            var dto = new AccountDto().FromJson(json, accountTypeDto);
            return dto;
        }
        
        public static AccountJson ToJson(this AccountDto dto)
        {
            // TODO: Delete this method
            return new AccountJson
            {
                Id = dto.Id,
                Email = dto.Email,
                Password = ""
            };
        }

        public static AccountNoPasswordJson ToNoPasswordJson(this AccountDto dto)
        {
            return new AccountNoPasswordJson
            {
                Id = dto.Id,
                Email = dto.Email
            };
        }
    }
}