using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// Account controller. To log in and manage account
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/[controller]")]
    public class AccountController : BaseController
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IAccountOrchestrator _accountOrchestrator;

        /// <summary>
        /// Initializes a new instance
        /// </summary>
        /// <param name="logger">Logger.</param>
        /// <param name="accountOrchestrator"></param>
        public AccountController(ILogger<AccountController> logger, IAccountOrchestrator accountOrchestrator) : base(logger)
        {
            this._logger = logger;
            _accountOrchestrator = accountOrchestrator;
        }

        /// <summary>
        /// Return account info from email and password
        /// </summary>
        /// <returns>In returns</returns>
        [HttpPost("login")]
        public async Task<ActionResult<AccountInfoJson>> Authenticate([FromBody] AccountCredentialsJson accountCredentialsJson)
        {
            try
            {
                var token = await _accountOrchestrator.Authenticate(accountCredentialsJson);
                return this.Ok(token);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Get all account type.
        /// </summary>
        /// <returns></returns>
        [HttpGet("types")]
        public async Task<ActionResult<IEnumerable<TypeJson>>> GetAllAccountType()
        {
            try
            {
                var accountTypes = await _accountOrchestrator.GetAllAccountTypes();
                return this.Ok(accountTypes);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Create the admin account and a store.
        /// </summary>
        /// <param name="businessJson"></param>
        /// <returns></returns>
        [HttpPost("register/business")]
        public async Task<ActionResult<AccountInfoJson>> CreateBusiness([FromBody] CreateBusinessJson businessJson)
        {
            try
            {
                var accountInfo = await _accountOrchestrator.CreateBusiness(businessJson);
                return this.Ok(accountInfo);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}