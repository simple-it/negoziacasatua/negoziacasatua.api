using System;
using System.Collections.Generic;
using System.Linq;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.DataModel.Factories
{
    public static class StoreDtoFactory
    {
        private static void ValidateDto(StoreDto dto)
        {
            new StringValidator(dto.Name, "Devi inserire un nome!");
            new StringValidator(dto.Description, "Devi inserire una descrizione!");
        }
        public static StoreLiteJson ToLiteJson(this StoreDto dto)
        {
            return new StoreLiteJson
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                City = dto.City,
                Address = dto.Address
            };
        }
        
        public static StoreAdminJson ToAdminJson(this StoreDto dto)
        {
            return new StoreAdminJson
            {
                Id = dto.Id,
                Name = dto.Name,
                Description = dto.Description,
                OwnerEmail = dto.Owner.Email
            };
        }
        
        public static StoreJson ToJson(this StoreDto dto, string baseImagePath)
        {
            var images = new List<ImageJson>();
            if(!string.IsNullOrEmpty(dto.PrimaryImagePath))
                images.Add(new ImageJson
                {
                    Id = -1,
                    Path = baseImagePath + dto.PrimaryImagePath
                });
            if(dto.Images != null) 
                images.AddRange(dto.Images.Select(t => new ImageJson
                {
                    Id = t.Id,
                    Path = baseImagePath + t.Path
                }));
            var contacts = dto.Contacts.Select(t => new ContactJson
            {
                Id = t.Id,
                Name = t.Name,
                Url = t.ContactType.BaseUrlPath + t.Name,
                ContactTypeId = t.ContactType.Id
            });
            var paymentTypes = dto.Payments.Select(t => new TypeJson
            {
                Id = t.PaymentType.Id,
                Name = t.PaymentType.Name
            });
            
            return new StoreJson
            {
                Id = dto.Id,
                Name = dto.Name,
                CategoryId = dto.Category.Id,
                CategoryName = dto.Category.Name,
                Description = dto.Description,
                DeliveryDetails = dto.DeliveryDetails,
                DeliveryConditions = dto.DeliveryConditions,
                ContactNote = dto.ContactNote,
                WebsiteLink = dto.WebsiteLink,
                CreationDate = dto.CreationDate,
                LastEditDate = dto.LastEditDate,
                City = dto.City,
                Address = dto.Address,
                ImagesPath = images,
                Contacts = contacts,
                PaymentTypes = paymentTypes
            };
        }

        public static StoreDto CreateFromJson(StoreToCreateJson json, AccountDto owner, IEnumerable<ContactDto> contacts, 
            IEnumerable<PaymentDto> payments, CategoryDto category, string primaryImagePath,
            IEnumerable<ImageDto> images, bool validated)
        {
            var dto = new StoreDto
            {
                Owner = owner, 
                CreationDate = DateTime.Now,
                PrimaryImagePath = primaryImagePath,
                Category = category
            };
            dto.EditFromJson(json, validated);
            dto.Images = images.Select(t =>
            {
                t.Store = dto;
                return t;
            }).ToList();
            dto.Contacts = contacts.Select(t =>
            {
                t.Store = dto;
                return t;
            }).ToList();
            dto.Payments = payments.Select(t =>
            {
                t.Store = dto;
                return t;
            }).ToList();
            
            return dto;
        }

        public static StoreDto EditFromJson(this StoreDto dto, StoreToCreateJson json, bool validated)
        {
            dto.Name = json.Name;
            dto.LastEditDate = DateTime.Now;
            dto.ContactNote = json.ContactNote;
            dto.WebsiteLink = json.WebsiteLink;
            dto.Description = json.Description;
            dto.DeliveryDetails = json.DeliveryDetails;
            dto.DeliveryConditions = json.DeliveryConditions;
            dto.Validated = validated;
            dto.City = json.City;
            dto.Address = json.Address;
            
            ValidateDto(dto);
            return dto;
        }

        public static StoreDto EditFromJson(this StoreDto dto, StoreToEditJson json, bool validated)
        {
            dto.Name = json.Name;
            dto.LastEditDate = DateTime.Now;
            dto.ContactNote = json.ContactNote;
            dto.WebsiteLink = json.WebsiteLink;
            dto.Description = json.Description;
            dto.DeliveryDetails = json.DeliveryDetails;
            dto.DeliveryConditions = json.DeliveryConditions;
            dto.Validated = validated;
            dto.City = json.City;
            dto.Address = json.Address;
            
            ValidateDto(dto);
            return dto;
        }
    }
}