using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class CategoryDto : DtoRoot
    {
        public string Name { get; set; }
        public bool Validated { get; set; }
        public string ImagePath { get; set; }
        
        public ICollection<StoreDto> Stores { get; set; }
    }
}