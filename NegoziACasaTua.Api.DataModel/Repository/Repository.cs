﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Facade;
using Microsoft.EntityFrameworkCore;

namespace NegoziACasaTua.Api.DataModel.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : DtoBase
    {
        private readonly DataModelFacade _dataModelFacade;
        internal DbSet<TEntity> DbSet;

        public Repository(DataModelFacade dataModelFacade)
        {
            this._dataModelFacade = dataModelFacade;
            this.DbSet = dataModelFacade.Set<TEntity>();
        }

        public async Task Insert(TEntity entityToAdd)
        {
            await this.DbSet.AddAsync(entityToAdd);
        }

        public void Update(TEntity entityToUpdate)
        {
            this.DbSet.Attach(entityToUpdate);
            this._dataModelFacade.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public async Task Delete(int id)
        {
            var entityToDelete = await this.DbSet.FindAsync(id);

            if (entityToDelete != null)
                this.Delete(entityToDelete);
        }

        public void Delete(TEntity entityToDelete)
        {
            if (this._dataModelFacade.Entry(entityToDelete).State == EntityState.Detached)
            {
                this.DbSet.Attach(entityToDelete);
            }

            this.DbSet.Remove(entityToDelete);
        }

        public void RemoveRange(IEnumerable<TEntity> entitiesToRemove)
        {
            this.DbSet.RemoveRange(entitiesToRemove);
        }

        public IQueryable<TEntity> GetQueryable()
        {
            return DbSet;
        }

//        public async Task<TEntity> GetByIdAsync(int id, string includeProperties = "")
//        {
//            includeProperties = includeProperties.Replace(" ", "");
//            IQueryable<TEntity> query = this.DbSet;
//
//            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
//                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
//
//            return await query.FirstOrDefaultAsync(q => q.Id == id);
//        }
//
//        public async Task<IEnumerable<TEntity>> GetAllAsync(int pageIndex, int pageSize, string includeProperties = "")
//        {
//            try
//            {
//                includeProperties = includeProperties.Replace(" ", "");
//                IQueryable<TEntity> query = this.DbSet;
//
//                query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
//                    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
//
//                if (pageSize > 0)
//                    query = query.OrderBy(q => q.Id).Skip(pageIndex * pageSize).Take(pageSize);
//
//                return await query.ToListAsync();
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex);
//                return Enumerable.Empty<TEntity>();
//            }
//        }
//
//        public async Task<IEnumerable<TEntity>> QueryAsync(Expression<Func<TEntity, bool>> filter = null,
//            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
//        {
//            includeProperties = includeProperties.Replace(" ", "");
//            IQueryable<TEntity> query = this.DbSet;
//
//            try
//            {
//                query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
//                    .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
//
//                if (filter != null)
//                    query = query.Where(filter);
//
//                return await query.ToListAsync();
//            }
//            catch (Exception ex)
//            {
//                Console.WriteLine(ex.Message);
//                return Enumerable.Empty<TEntity>();
//            }
//        }
    }
}