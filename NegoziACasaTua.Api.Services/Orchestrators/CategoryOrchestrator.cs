using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Services.Orchestrators
{
    public class CategoryOrchestrator : ICategoryOrchestrator
    {
        private readonly IStoreService _storeService;
        private readonly ICategoryService _categoryService;

        public CategoryOrchestrator(IStoreService storeService, ICategoryService categoryService)
        {
            _storeService = storeService;
            _categoryService = categoryService;
        }

        public async Task<IEnumerable<CategoryJson>> GetAllCategories()
        {
            return await _categoryService.GetAllCategoriesAsync();
        }
    }
}