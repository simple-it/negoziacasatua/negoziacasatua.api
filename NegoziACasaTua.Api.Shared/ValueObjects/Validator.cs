using System;
using System.Linq.Expressions;

namespace NegoziACasaTua.Api.Shared.ValueObjects
{
    public class Validator<T>
    {
        protected T obj;

        protected Validator(T obj)
        {
            this.obj = obj;
        }

        public Validator(T obj, string message)
        {
            if(obj == null)
                throw new Exception(message);
            this.obj = obj;
        }
    }
}