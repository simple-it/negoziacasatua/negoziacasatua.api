using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NegoziACasaTua.Api.DataModel.Dtos;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class ContactTypeMapping : IEntityTypeConfiguration<ContactTypeDto>
    {
        public void Configure(EntityTypeBuilder<ContactTypeDto> builder)
        {
            builder.ToTable("ContactType");

            builder.HasMany(t => t.Contacts)
                .WithOne(t => t.ContactType);

            builder.Property(t => t.Id);
            builder.Property(t => t.Name);
            builder.Property(t => t.BaseUrlPath);
        }
    }
}