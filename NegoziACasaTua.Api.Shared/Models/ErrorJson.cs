namespace NegoziACasaTua.Api.Shared.Models
{
    public class ErrorJson
    {
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
    }
}