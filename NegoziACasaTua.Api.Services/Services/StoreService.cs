using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.DataModel.Factories;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.Services.Services
{
    public class StoreService : IStoreService
    {
        private readonly ILogger<StoreService> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _settings;
        private readonly IImageService _imageService;

        public StoreService(IUnitOfWork unitOfWork, IOptions<AppSettings> settings, ILogger<StoreService> logger, IImageService imageService)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _imageService = imageService;
            _settings = settings.Value;
        }

        public async Task CreateStore(StoreToCreateJson storeJson, int ownerId, bool validated = false)
        {
            CheckJson(storeJson);
            var owner = await GetOwnerDto(ownerId);
            if (owner.Store != null)
                throw new Exception("Il negozio è già stato creato!");

            var imagesTask = StartUploadingImagesFromJson(storeJson);
            var categoryDto = await GetCategoryDto(storeJson.CategoryId);
            var contactsDto = await GetContactsDto(storeJson.Contacts);
            var paymentsDto = await GetPaymentsDto(storeJson.PaymentTypesId);

            string primaryImagePath = null;
            if (!string.IsNullOrEmpty(storeJson.PrimaryImageBase64))
            {
                var first = imagesTask.First();
                imagesTask.RemoveAt(0);
                primaryImagePath = await first;
            }
            
            var images = (await Task.WhenAll(imagesTask)).Select(t => new ImageDto
            {
                Path = t
            });

            var storeDto = StoreDtoFactory.CreateFromJson(storeJson, owner, contactsDto, paymentsDto,
                categoryDto, primaryImagePath, images, validated);
            await _unitOfWork.StoreRepository.Insert(storeDto);
        }

        public async Task EditStore(int storeId, StoreToEditJson storeJson, bool validated = false)
        {
            CheckJson(storeJson);
            var storeDto = await _unitOfWork.StoreRepository.GetQueryable().Include(t => t.Owner)
                .Include(t => t.Category).Include(t => t.Images)
                .Include(t => t.Contacts).ThenInclude(t => t.ContactType)
                .Include(t => t.Payments).ThenInclude(t => t.PaymentType)
                .FirstOrDefaultAsync(t => t.Id == storeId);

            // Start uploading images
            Task<string> primaryImageTask = null;
            IEnumerable<Task<ImageDto>> imagesTask = null;
            if (storeJson.Images != null && storeJson.Images.Any())
            {
                var primaryImage = storeJson.Images.First();
                if (primaryImage.Id == 0)
                    primaryImageTask = _imageService.UploadImageAndGetPathAsync(primaryImage.Path);
                var imageJsons = storeJson.Images.Skip(1);
                imagesTask = StartUploadingImagesFromJson(imageJsons, storeDto.Images);
            }
            
            // Update category
            storeDto.Category = storeJson.CategoryId == storeDto.Category.Id
                ? storeDto.Category
                : await GetCategoryDto(storeJson.CategoryId);

            // Check the new added contacts
            var contactsToAdd = storeJson.Contacts.Where(c =>
                storeDto.Contacts.All(dto => dto.Id != c.Id || 
                                             dto.ContactType.Id != c.ContactTypeId || 
                                             dto.Name != c.Name));
            var contactsToAddDtoTask = GetContactsDto(contactsToAdd);
            // Filter to keep existing contacts that remain
            var contactsDto = new List<ContactDto>();
            foreach (var contact in storeDto.Contacts)
            {
                if (storeJson.Contacts.Any(c => c.Id == contact.Id && 
                                                c.ContactTypeId == contact.ContactType.Id &&
                                                c.Name == contact.Name))
                    contactsDto.Add(contact);
                else
                    _unitOfWork.ContactRepository.Delete(contact);
            }
            contactsDto.AddRange(await contactsToAddDtoTask);
            storeDto.Contacts = contactsDto;
            
            // Check the new added payment types
            var paymentTypesToAdd = storeJson.PaymentTypesId.Where(c =>
                storeDto.Payments.All(dto => dto.PaymentType.Id != c));
            var paymentsToAddDtoTask = GetPaymentsDto(paymentTypesToAdd);
            // Filter to keep existing contacts that remain
            var paymentsDto = storeDto.Payments.Where(p =>
            {
                var cond = storeJson.PaymentTypesId.Any(pType => p.PaymentType.Id == pType);
                if (!cond)
                    _unitOfWork.PaymentRepository.Delete(p);
                return cond;
            });
            paymentsDto = paymentsDto.Concat(await paymentsToAddDtoTask);
            storeDto.Payments = paymentsDto.ToList();
            
            // Await for images to complete upload
            if (primaryImageTask != null)
            {
                await _imageService.DeleteIfExists(storeDto.PrimaryImagePath);
                storeDto.PrimaryImagePath = await primaryImageTask;
            }

            if (imagesTask != null)
                storeDto.Images = (await Task.WhenAll(imagesTask)).ToList();

            storeDto.EditFromJson(storeJson, validated);
            _unitOfWork.StoreRepository.Update(storeDto);
        }

        public async Task EditOwnerEmail(int storeId, EmailJson emailJson)
        {
            new StringValidator(emailJson.Email, "Devi inserire una mail!")
                .IsEmail("Email non valida!");
            
            var store = await _unitOfWork.StoreRepository.GetQueryable().Include(t => t.Owner)
                .FirstOrDefaultAsync(t => t.Id == storeId);
            if(store == null)
                throw new Exception("Nessun negozio trovato!");

            var user = await _unitOfWork.AccountRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Email == emailJson.Email);
            if(user != null)
                throw new Exception("L'email è già stata usata!");

            var owner = store.Owner;
            owner.Email = emailJson.Email;
            owner.Password = ""; // TODO: Send mail to user with the password
            _unitOfWork.AccountRepository.Update(owner);
        }

        public async Task ValidateStoreById(int id, bool validate = true)
        {
            var store = await _unitOfWork.StoreRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == id);
            if(store == null)
                throw new Exception("Negozio non trovato!");
            if(store.Validated == validate)
                return;
            
            store.Validated = validate;
            _unitOfWork.StoreRepository.Update(store);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteStoreAndOwner(int id)
        {
            var store = await _unitOfWork.StoreRepository.GetQueryable().Include(t => t.Owner)
                .Include(t => t.Payments).Include(t => t.Contacts)
                .Include(t => t.Images).FirstOrDefaultAsync(t => t.Id == id);

            foreach (var image in store.Images)
            {
                await _imageService.DeleteIfExists(image.Path);
                _unitOfWork.ImageRepository.Delete(image);
            }
            foreach (var contact in store.Contacts)
            {
                _unitOfWork.ContactRepository.Delete(contact);
            }
            foreach (var payment in store.Payments)
            {
                _unitOfWork.PaymentRepository.Delete(payment);
            }

            _unitOfWork.AccountRepository.Delete(store.Owner);
            _unitOfWork.StoreRepository.Delete(store);
        }

        public async Task MarkStoreAsDeleted(int id)
        {
            var store = await _unitOfWork.StoreRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == id && t.DeletedDate == null);
            if(store == null)
                throw new Exception("Negozio non trovato");
            
            store.DeletedDate = DateTime.Now;
            _unitOfWork.StoreRepository.Update(store);
        }
        
        

        private List<Task<string>> StartUploadingImagesFromJson(StoreToCreateJson json)
        {
            var imagesNumber = 0;
            var imagesTask = new List<Task<string>>();
            
            if(!string.IsNullOrEmpty(json.PrimaryImageBase64))
                imagesTask.Add(_imageService.UploadImageAndGetPathAsync(
                    json.PrimaryImageBase64, ++imagesNumber));
            
            if (json.ImagesBase64 != null && json.ImagesBase64.Count() != 0)
            {
                imagesTask.AddRange(json.ImagesBase64.Select(base64 => 
                    _imageService.UploadImageAndGetPathAsync(base64, ++imagesNumber)));
            }

            return imagesTask;
        }
        
        private IEnumerable<Task<ImageDto>> StartUploadingImagesFromJson(IEnumerable<ImageJson> jsons, ICollection<ImageDto> images)
        {
            var imageJsons = jsons as ImageJson[] ?? jsons.ToArray();
            var imagesTaskDto = imageJsons.Select(async json =>
            {
                if (json.Id == 0)
                    return new ImageDto
                    {
                        Path = await _imageService.UploadImageAndGetPathAsync(json.Path)
                    };
                
                return images.First(i => i.Id == json.Id);
            });

            foreach (var image in images)
                if (imageJsons.All(t => t.Id != image.Id))
                    _unitOfWork.ImageRepository.Delete(image);
            
            return imagesTaskDto;
        }

        private async Task<IEnumerable<ContactDto>> GetContactsDto(IEnumerable<ContactToCreateJson> contacts)
        {
            var contactTypesDto = await _unitOfWork.ContactTypeRepository.GetQueryable()
                .ToListAsync();
            var contactsDto = contacts.Select(contact =>
            {
                contact.Id = 0;
                var contactType = contactTypesDto.FirstOrDefault(t => t.Id == contact.ContactTypeId);
                if(contactType == null)
                    throw new Exception("Un tipo di contatto non è stato trovato!");
                return ContactDtoFactory.FromJson(contact, contactType);
            });
            return contactsDto;
        }

        private async Task<IEnumerable<PaymentDto>> GetPaymentsDto(IEnumerable<int> paymentTypesId)
        {
            var paymentTypesDto = await _unitOfWork.PaymentTypeRepository.GetQueryable()
                .ToListAsync();
            var paymentsDto = paymentTypesId.Select(paymentTypeId =>
            {
                var paymentTypeDto = paymentTypesDto.FirstOrDefault(t => t.Id == paymentTypeId);
                if (paymentTypeDto == null)
                    throw new Exception("Un tipo di pagamento non è stato trovato!");
                return new PaymentDto
                {
                    PaymentType = paymentTypeDto
                };
            });
            return paymentsDto;
        }

        private async Task<CategoryDto> GetCategoryDto(int categoryId)
        {
            var categoryDto = await _unitOfWork.CategoryRepository.GetQueryable()
                .FirstOrDefaultAsync(t => t.Id == categoryId);
            if(categoryDto == null)
                throw new Exception("La categoria non è stata trovata!");
            return categoryDto;
        }

        private void CheckJson(StoreToCreateJson json)
        {
            if (json == null) throw new ArgumentNullException(nameof(json));
            if(!(json.Contacts != null && json.Contacts.Count() != 0))
                throw new Exception("Devi inserire almeno un contatto!");
            if(!(json.PaymentTypesId != null && json.PaymentTypesId.Count() != 0))
                throw new Exception("Devi inserire almeno un metodo di pagamento!");
        }
        
        private void CheckJson(StoreToEditJson json)
        {
            if (json == null) throw new ArgumentNullException(nameof(json));
            if(!(json.Contacts != null && json.Contacts.Count() != 0))
                throw new Exception("Devi inserire almeno un contatto!");
            if(!(json.PaymentTypesId != null && json.PaymentTypesId.Count() != 0))
                throw new Exception("Devi inserire almeno un metodo di pagamento!");
        }

        private async Task<AccountDto> GetOwnerDto(int ownerId)
        {
            var owner = await _unitOfWork.AccountRepository.GetQueryable()
                .Include(t => t.Store)
                .FirstOrDefaultAsync(t => t.Id == ownerId);
            if(owner == null)
                throw new Exception("Utente non trovato!");
            return owner;
        }
    }
}