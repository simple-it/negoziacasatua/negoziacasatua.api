using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class ContactTypeDto : DtoRoot
    {
        public string Name { get; set; }
        public string BaseUrlPath { get; set; }
        
        public ICollection<ContactDto> Contacts { get; set; }
    }
}