using System;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class AccountInfoJson
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int StoreId { get; set; }
        public string StoreName { get; set; }
        public int AccountTypeId { get; set; }
        public string Token { get; set; }
        public DateTime TokenExpiration { get; set; }
    }
}