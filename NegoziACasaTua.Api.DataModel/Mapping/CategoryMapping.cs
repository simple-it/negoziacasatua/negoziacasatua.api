using NegoziACasaTua.Api.DataModel.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class CategoryMapping : IEntityTypeConfiguration<CategoryDto>
    {
        public void Configure(EntityTypeBuilder<CategoryDto> builder)
        {
            builder.ToTable("Category");
            
            builder.HasMany(t => t.Stores)
                .WithOne(t => t.Category);

            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Name).HasMaxLength(32).IsRequired();
            builder.Property(t => t.ImagePath);
        }
    }
}