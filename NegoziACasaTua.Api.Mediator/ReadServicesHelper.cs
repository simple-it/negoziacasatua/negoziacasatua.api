using Microsoft.Extensions.DependencyInjection;
using NegoziACasaTua.Api.Services.ReadModelServices;
using NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices;

namespace NegoziACasaTua.Api.Mediator
{
    public static class ReadServicesHelper
    {
        public static IServiceCollection AddReadServices(this IServiceCollection services)
        {
            // Transient = created each time they're requested from the service container
            // Scoped = created once per client request
            // Singleton
            services.AddScoped<IStoreReadService, StoreReadService>();

            return services;
        }
    }
}