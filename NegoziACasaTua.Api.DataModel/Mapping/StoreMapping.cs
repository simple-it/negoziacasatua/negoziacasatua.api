using NegoziACasaTua.Api.DataModel.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class StoreMapping : IEntityTypeConfiguration<StoreDto>
    {
        public void Configure(EntityTypeBuilder<StoreDto> builder)
        {
            builder.ToTable("Store");

            builder.HasOne(t => t.Owner)
                .WithOne(t => t.Store)
                .HasForeignKey<StoreDto>(t => t.OwnerId);
            builder.HasMany(t => t.Images)
                .WithOne(t => t.Store);
            builder.HasMany(t => t.Contacts)
                .WithOne(t => t.Store);
            builder.HasMany(t => t.Contacts)
                .WithOne(t => t.Store);
            builder.HasMany(t => t.Payments)
                .WithOne(t => t.Store);

            
            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Name).IsRequired();
            builder.Property(t => t.Description);
            builder.Property(t => t.DeliveryDetails);
            builder.Property(t => t.DeliveryConditions);
            builder.Property(t => t.CreationDate);
            builder.Property(t => t.LastEditDate);
            builder.Property(t => t.DeletedDate);
            builder.Property(t => t.PrimaryImagePath);
            builder.Property(t => t.Validated);
            builder.Property(t => t.ContactNote);

            builder.Property(t => t.City);
            builder.Property(t => t.Address);
        }
    }
}