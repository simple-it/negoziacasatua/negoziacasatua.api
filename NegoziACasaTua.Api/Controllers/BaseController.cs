﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace NegoziACasaTua.Api.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Common Controller to share functions/methods
    /// </summary>
    [ApiController]
    [Route("api")]
    public class BaseController : Controller
    {
        private readonly ILogger<BaseController> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public BaseController(ILogger<BaseController> logger)
        {
            _logger = logger;
        }

        internal ActionResult AnswerWithError(Exception ex)
        {
            _logger.LogError(ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            return BadRequest(new ErrorJson
            {
                Error = ex.InnerException != null ? ex.InnerException.Message : ex.Message
            });
        }

        internal string GetClaim(string claimType)
        {
            return User.Claims.FirstOrDefault(t => t.Type == claimType)?.Value;
        }
    }
}