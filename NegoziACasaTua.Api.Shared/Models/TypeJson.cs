namespace NegoziACasaTua.Api.Shared.Models
{
    public class TypeJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}