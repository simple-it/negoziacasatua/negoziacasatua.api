using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NegoziACasaTua.Api.DataModel.Dtos;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class PaymentTypeMapping : IEntityTypeConfiguration<PaymentTypeDto>
    {
        public void Configure(EntityTypeBuilder<PaymentTypeDto> builder)
        {
            builder.ToTable("PaymentType");

            builder.HasMany(t => t.Payments)
                .WithOne(t => t.PaymentType);

            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Name);
        }
    }
}