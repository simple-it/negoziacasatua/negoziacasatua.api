namespace NegoziACasaTua.Api.Shared.Models
{
    public class ContactJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        /// <summary>A link to open de contact directly (create a mail, open a number on whatsapp...)</summary>
        public string Url { get; set; }
        public int ContactTypeId { get; set; }
    }
}