namespace NegoziACasaTua.Api.Shared.Models
{
    public class CategoryToCreate
    {
        public string Name { get; set; }
        public string Base64 { get; set; }
    }
}