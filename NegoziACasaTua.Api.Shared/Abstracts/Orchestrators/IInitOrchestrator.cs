using System.Threading.Tasks;

namespace NegoziACasaTua.Api.Shared.Abstracts.Orchestrators
{
    public interface IInitOrchestrator
    {
        Task Init();
    }
}