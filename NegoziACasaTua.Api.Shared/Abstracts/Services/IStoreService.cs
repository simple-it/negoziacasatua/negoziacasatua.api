using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface IStoreService
    {
        Task CreateStore(StoreToCreateJson storeJson, int ownerId, bool validated = false);
        Task EditStore(int storeId, StoreToEditJson storeJson, bool validated = false);
        
        Task EditOwnerEmail(int storeId, EmailJson emailJson);
        Task ValidateStoreById(int id, bool validate = true);
        Task DeleteStoreAndOwner(int id);
        Task MarkStoreAsDeleted(int id);
        
    }
}