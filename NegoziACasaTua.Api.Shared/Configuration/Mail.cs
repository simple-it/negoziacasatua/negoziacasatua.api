namespace NegoziACasaTua.Api.Shared.Configuration
{
    public class Mail
    {
        public string SendGridApiKeyVarName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderEmailName { get; set; }
    }
}