using NegoziACasaTua.Api.DataModel.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class AccountMapping : IEntityTypeConfiguration<AccountDto>
    {
        public void Configure(EntityTypeBuilder<AccountDto> builder)
        {
            builder.ToTable("Account");

            builder.HasOne(t => t.AccountType)
                .WithMany(t => t.Accounts)
                .IsRequired();
            builder.HasOne(t => t.Store)
                .WithOne(t => t.Owner);

            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Email).HasMaxLength(64);
            builder.Property(t => t.Password).HasMaxLength(64);  //  SHA256 length
            builder.Property(t => t.MailConfirmed).IsRequired();
        }
    }
}