using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/Account/admin")]
    public class AccountAdminController : BaseController
    {
        private readonly IAccountOrchestrator _accountOrchestrator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="accountOrchestrator"></param>
        public AccountAdminController(ILogger<BaseController> logger, IAccountOrchestrator accountOrchestrator) : base(logger)
        {
            _accountOrchestrator = accountOrchestrator;
        }
        
        
        /// <summary>
        /// Require admin. Get all accounts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "1")]
        public async Task<ActionResult<IEnumerable<AccountJson>>> GetAllAccounts()
        {
            try
            {
                var accounts = await _accountOrchestrator.GetAllAccounts();
                return this.Ok(accounts);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require admin. Get all account type with all relative accounts.
        /// </summary>
        /// <returns></returns>
        [HttpGet("types")]
        [Authorize(Roles = "1")]
        public async Task<ActionResult<IEnumerable<AccountTypeJson>>> GetAllAccountTypeWithAccounts()
        {
            try
            {
                var accountTypes = await _accountOrchestrator.GetAllAccountTypesWithAccounts();
                return this.Ok(accountTypes);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Require login. Get user by id (each user only personal account and admin all of them)
        /// </summary>
        /// <returns></returns>
        [HttpGet("{userId}")]
        [Authorize]
        public async Task<ActionResult<AccountJson>> GetUserById([FromRoute] int userId)
        {
            try
            {
                if (User.Identity.Name != userId.ToString() && !User.IsInRole("1"))
                {
                    return Forbid();
                }
                
                var user = await _accountOrchestrator.GetAccountById(userId);
                return this.Ok(user);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Require admin. Create an user
        /// </summary>
        /// <param name="accountJson"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        [HttpPost("register/accountType/{accountType}")]
        [Authorize(Roles = "1")]
        public async Task<ActionResult> CreateAccount([FromBody] AccountCredentialsJson accountJson, [FromRoute] int accountType)
        {
            try
            {
                await _accountOrchestrator.CreateAccount(accountJson, accountType);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Require login (admin). Edit an user (if the password property is empty, it will not be changed)
        /// </summary>
        /// <param name="accountJson"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "1")]
        public async Task<IActionResult> UpdateAccount([FromBody] AccountJson accountJson)
        {
            try
            {
                await _accountOrchestrator.UpdateAccount(accountJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }

        /// <summary>
        /// Require login. Edit password (only personal -> account id is taken from token (ahahahahah))
        /// </summary>
        /// <param name="accountJson"></param>
        /// <returns></returns>
        [HttpPut("password")]
        [Authorize]
        public async Task<IActionResult> UpdatePassword([FromBody] PasswordJson accountJson)
        {
            try
            {
                await _accountOrchestrator.UpdatePassword(int.Parse(User.Identity.Name), accountJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login. Delete user by id (each user only personal account and admin all of them)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        [Authorize]
        public async Task<IActionResult> DeleteAccountById([FromRoute] int userId)
        {
            try
            {
                if (User.Identity.Name != userId.ToString() && !User.IsInRole("1"))
                {
                    return Forbid();
                }
                
                await _accountOrchestrator.DeleteAccountById(userId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin). Create the store and an admin account if email is inserted.
        /// </summary>
        /// <param name="businessJson"></param>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpPost("register")]
        public async Task<ActionResult> CreateBusinessWithoutPassword([FromBody] CreateBusinessJson businessJson)
        {
            try
            {
                await _accountOrchestrator.CreateBusinessByAdmin(businessJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}