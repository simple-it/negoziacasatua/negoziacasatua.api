using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Shared.Abstracts.Orchestrators
{
    public interface ICategoryOrchestrator
    {
        Task<IEnumerable<CategoryJson>> GetAllCategories();
    }
}