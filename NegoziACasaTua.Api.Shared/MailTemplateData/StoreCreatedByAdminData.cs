namespace NegoziACasaTua.Api.Shared.MailTemplateData
{
    public class StoreCreatedByAdminData
    {
        public string StoreName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}