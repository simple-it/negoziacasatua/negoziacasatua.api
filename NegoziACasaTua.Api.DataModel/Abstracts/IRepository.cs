﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NegoziACasaTua.Api.DataModel.Abstracts
{
    public interface IRepository<TEntity> where TEntity : DtoBase
    {
        Task Insert(TEntity entityToAdd);
        void Update(TEntity entityToUpdate);
        Task Delete(int id);
        void Delete(TEntity entityToDelete);
        void RemoveRange(IEnumerable<TEntity> entitiesToRemove);
        IQueryable<TEntity> GetQueryable();
    }
}