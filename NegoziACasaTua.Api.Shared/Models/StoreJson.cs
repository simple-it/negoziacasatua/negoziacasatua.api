using System;
using System.Collections.Generic;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class StoreJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string DeliveryDetails { get; set; }
        public string DeliveryConditions { get; set; }
        public string ContactNote { get; set; }
        public string WebsiteLink { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastEditDate { get; set; }

        public IEnumerable<ContactJson> Contacts { get; set; }
        public IEnumerable<TypeJson> PaymentTypes { get; set; }
        
        public string City { get; set; }
        public string Address { get; set; }

        public IEnumerable<ImageJson> ImagesPath { get; set; }
    }
}