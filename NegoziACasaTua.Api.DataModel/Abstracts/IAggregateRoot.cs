﻿namespace NegoziACasaTua.Api.DataModel.Abstracts
{
    public interface IAggregateRoot
    {
        int Id { get; }
    }
}