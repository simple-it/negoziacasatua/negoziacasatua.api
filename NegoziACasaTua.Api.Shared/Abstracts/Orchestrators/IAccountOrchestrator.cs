using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Shared.Abstracts.Orchestrators
{
    public interface IAccountOrchestrator
    {
        Task<AccountInfoJson> Authenticate(AccountCredentialsJson accountCredentialsJson);
        Task<IEnumerable<AccountJson>> GetAllAccounts();
        Task<AccountJson> GetAccountById(int accountId);
        Task CreateAccount(AccountCredentialsJson accountJson, int accountType);
        Task UpdateAccount(AccountJson accountJson);
        Task UpdatePassword(int accountId, PasswordJson password);
        Task DeleteAccountById(int accountId);
        Task<IEnumerable<TypeJson>> GetAllAccountTypes();
        Task<IEnumerable<AccountTypeJson>> GetAllAccountTypesWithAccounts();
        
        Task<AccountInfoJson> CreateBusiness(CreateBusinessJson businessJson);
        Task CreateBusinessByAdmin(CreateBusinessJson businessJson);
    }
}