using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.DataModel.Factories
{
    public static class AccountTypeFactory
    {
        public static AccountTypeJson ToJson(this AccountTypeDto dto, IEnumerable<AccountNoPasswordJson> accounts)
        {
            var json = new AccountTypeJson
            {
                Id = dto.Id,
                Name = dto.Name,
                Accounts = accounts
            };

            return json;
        }
    }
}