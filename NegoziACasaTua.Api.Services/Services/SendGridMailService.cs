using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.MailTemplateData;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace NegoziACasaTua.Api.Services.Services
{
    public class SendGridMailService : IMailService
    {
        private readonly SendGridClient _client;
        private readonly EmailAddress _from;
        
        private const string SendPasswordUserTemplate = "d-04546ebef5ef4da0bbe2846b2a453974";

        public SendGridMailService(IOptions<AppSettings> options)
        {
            var settings = options.Value;
            var apiKey = Environment.GetEnvironmentVariable(settings.Mail.SendGridApiKeyVarName);
            _client = new SendGridClient(apiKey);

            _from = new EmailAddress(settings.Mail.SenderEmail, settings.Mail.SenderEmailName);
        }

        public async Task SendStoreCreatedByAdminMail(string toEmail, StoreCreatedByAdminData data)
        {
            var to = new EmailAddress(toEmail);
            var email = MailHelper.CreateSingleTemplateEmail(_from, to, SendPasswordUserTemplate, data);
            await _client.SendEmailAsync(email);
        }

        public async Task SendMail(string toEmail, string toName, string subject, string mailText, string mailHtml)
        {
            var to = new EmailAddress(toEmail, toName);
            var email = MailHelper.CreateSingleEmail(_from, to, subject, mailText, mailHtml);
            await _client.SendEmailAsync(email);
        }
    }
}