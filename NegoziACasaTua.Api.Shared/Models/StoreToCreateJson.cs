using System.Collections.Generic;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class StoreToCreateJson
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DeliveryDetails { get; set; }
        public string DeliveryConditions { get; set; }
        public int CategoryId { get; set; }
        public string ContactNote { get; set; }
        public string WebsiteLink { get; set; }
        
        public IEnumerable<ContactToCreateJson> Contacts { get; set; }
        public IEnumerable<int> PaymentTypesId { get; set; }
        
        public string City { get; set; }
        public string Address { get; set; }

        public string PrimaryImageBase64 { get; set; }
        public IEnumerable<string> ImagesBase64 { get; set; }
    }
}