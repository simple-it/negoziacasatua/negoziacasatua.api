using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Types;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.DataModel.Factories
{
    public static class ContactDtoFactory
    {
        public static void ValidateDto(ContactDto dto)
        {
            var nameValidator = new StringValidator(dto.Name, "Un contatto è vuoto!");

            dto.Name = dto.Name.Replace(" ", "");
            if (dto.ContactType.Id == ApiContactTypes.Email)
                nameValidator.IsEmail($"La mail '{dto.Name}' non è valida");
        }
        
        public static ContactDto FromJson(ContactToCreateJson json, ContactTypeDto contactTypeDto)
        {
            var dto = new ContactDto
            {
                Name = json.Name,
                ContactType = contactTypeDto
            };
            ValidateDto(dto);
            return dto;
        }
        
        public static ContactDto FromJson(this ContactDto dto, ContactToCreateJson json, ContactTypeDto contactTypeDto)
        {
            dto.Name = json.Name;
            dto.ContactType = contactTypeDto;
            ValidateDto(dto);
            return dto;
        }
    }
}