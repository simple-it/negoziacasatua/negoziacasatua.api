using System;
using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class StoreDto : DtoRoot
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string DeliveryDetails { get; set; }
        public string DeliveryConditions { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastEditDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string PrimaryImagePath { get; set; }
        public bool Validated { get; set; }
        public string ContactNote { get; set; }
        public string WebsiteLink { get; set; }

        public string City { get; set; }
        public string Address { get; set; }
        
        public int OwnerId { get; set; }
        public AccountDto Owner { get; set; }
        public CategoryDto Category { get; set; }
        public ICollection<PaymentDto> Payments { get; set; }
        public ICollection<ContactDto> Contacts { get; set; }
        public ICollection<ImageDto> Images { get; set; }
    }
}