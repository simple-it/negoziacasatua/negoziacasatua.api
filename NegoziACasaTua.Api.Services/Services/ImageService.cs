using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Services.Utilities;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.ValueObjects;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace NegoziACasaTua.Api.Services.Services
{
    public class ImageService : IImageService
    {
        private readonly AppSettings _settings;
        private readonly ILogger<ImageService> _logger;
        private readonly IStorageService _storageService;

        public ImageService(IOptions<AppSettings> settings, ILogger<ImageService> logger, IStorageService storageService)
        {
            _settings = settings.Value;
            _logger = logger;
            _storageService = storageService;
        }

        public async Task<string> UploadImageAndGetPathAsync(string base64, int imagesNumber = 0)
        {
            new StringValidator(base64, "L'immagine numero " + imagesNumber + " risulta vuota!");
            try
            {
                var imagePath = _settings.Storage.BasePathImages + Guid.NewGuid().ToString("N") + "." +
                                GetExtensionFromBase64(base64);
                await _storageService.UploadFile(imagePath, GetDataFromBase64(base64));
                return imagePath;
            }
            catch (Exception e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw new Exception("Errore durante il salvataggio delle immagini");
            }
        }

        private string GetExtensionFromBase64(string base64)
        {
            return Regex.Match(base64, @"^data:image/([a-zA-Z]+);base64").Groups[1].Value;
        }

        private string GetDataFromBase64(string base64)
        {
            return Regex.Replace(base64, @"^data:image\/[a-zA-Z]+;base64,", string.Empty);
        }

        public async Task DeleteIfExists(string path)
        {
            if (string.IsNullOrEmpty(path))
                return;

            await _storageService.DeleteFile(path);
        }

        public Task DeleteIfExists(IEnumerable<string> paths)
        {
            throw new NotImplementedException();
        }
    }
}