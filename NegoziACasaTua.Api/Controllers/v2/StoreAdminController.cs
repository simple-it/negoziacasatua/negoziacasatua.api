using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.Types;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/Store/Admin")]
    public class StoreAdminController : BaseController
    {
        private readonly IStoreOrchestrator _storeOrchestrator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="storeOrchestrator"></param>
        public StoreAdminController(ILogger<BaseController> logger, IStoreOrchestrator storeOrchestrator) : base(logger)
        {
            _storeOrchestrator = storeOrchestrator;
        }
        
        
        /// <summary>
        /// Require login (admin or municipality). Validate a store.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpGet("validate/{storeId}")]
        public async Task<ActionResult> ValidateStoreById([FromRoute] int storeId)
        {
            try
            {
                await _storeOrchestrator.ValidateStoreById(storeId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin or municipality). Add or edit owner email to a store (edit it! Be careful!).
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpPost("addowner/{storeId}")]
        public async Task<ActionResult> EditOwnerEmailById([FromRoute] int storeId, [FromBody] EmailJson emailJson)
        {
            try
            {
                await _storeOrchestrator.EditOwnerEmail(storeId, emailJson);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin or municipality). Mark a store as deleted.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpDelete("{storeId}")]
        public async Task<ActionResult> MarkStoreAsDeleted([FromRoute] int storeId)
        {
            try
            {
                await _storeOrchestrator.MarkStoreAsDeleted(storeId);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin or municipality). Edit the store by id
        /// </summary>
        /// <param name="storeJson"></param>
        /// <param name="storeId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut("{storeId}")]
        public async Task<ActionResult> EditStoreById([FromBody] StoreToEditJson storeJson, [FromRoute] int storeId)
        {
            try
            {
                var tokenStoreId = User.Claims.FirstOrDefault(t => t.Type == ApiClaimTypes.StoreId);
                var isAdmin = User.IsInRole(ApiAccountTypes.Admin.ToString()) ||
                              User.IsInRole(ApiAccountTypes.Municipality.ToString());
                var isOwner = tokenStoreId != null && tokenStoreId.Value == storeId.ToString(); 
                if (!(isAdmin || isOwner))
                    return Forbid();
                await _storeOrchestrator.EditStoreById(storeId, storeJson, isAdmin);
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}