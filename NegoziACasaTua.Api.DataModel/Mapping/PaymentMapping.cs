using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NegoziACasaTua.Api.DataModel.Dtos;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class PaymentMapping : IEntityTypeConfiguration<PaymentDto>
    {
        public void Configure(EntityTypeBuilder<PaymentDto> builder)
        {
            builder.ToTable("Payment");

            builder.HasOne(t => t.Store)
                .WithMany(t => t.Payments);
            builder.HasOne(t => t.PaymentType)
                .WithMany(t => t.Payments);

            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
        }
    }
}