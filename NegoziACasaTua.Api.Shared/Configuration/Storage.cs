namespace NegoziACasaTua.Api.Shared.Configuration
{
    public class Storage
    {
        public string CredentialJsonVariableName { get; set; }
        public string BasePathImages { get; set; }
        public string BasePathAppImages { get; set; }
        public string BucketName { get; set; }
        public string StorageBasePath { get; set; }
    }
}