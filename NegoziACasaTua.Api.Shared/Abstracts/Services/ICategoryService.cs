using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryJson>> GetAllCategoriesAsync();
//        Task CreateCategoryAsync(CategoryToCreate categoryToCreate);
//        Task EditCategoryAsync(int id, CategoryToCreate categoryToCreate);
    }
}