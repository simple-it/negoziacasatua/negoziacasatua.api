using System.Net.Mail;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.MailTemplateData;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface IMailService
    {
        Task SendStoreCreatedByAdminMail(string toEmail, StoreCreatedByAdminData data);
        Task SendMail(string toEmail, string toName, string subject, string mailText, string mailHtml);
    }
}