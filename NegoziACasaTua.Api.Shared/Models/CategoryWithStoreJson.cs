using System.Collections.Generic;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class CategoryWithStoreJson
    {
        public string Name { get; set; }
        public IEnumerable<StoreLiteJson> Stores { get; set; }
    }
}