using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices
{
    public interface IStoreReadService
    {
        Task<InformationTypeJson> GetAllInformationTypes();
        
        Task<IEnumerable<StoreAdminJson>> GetAllStores(bool validated = true);
        Task<ValueJson> GetStoresToValidateCount();
        Task<CategoryWithStoreJson> GetStoresByCategoryId(int categoryId);
        Task<StoreJson> GetStoreById(int id, bool isAdmin);
        Task<int> GetStoreIdByOwnerId(int ownerId);
        Task<StoreInfoJson> GetStoreInfo(int id);
        Task<IEnumerable<StoreLiteJson>> SearchStore(string search, int resultsLimit, int page);
    }
}