using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class ImageDto : DtoRoot
    {
        public string Path { get; set; }
        
        public StoreDto Store { get; set; }
    }
}