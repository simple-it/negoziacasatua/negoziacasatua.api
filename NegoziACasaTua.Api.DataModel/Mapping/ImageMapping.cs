using NegoziACasaTua.Api.DataModel.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class ImageMapping : IEntityTypeConfiguration<ImageDto>
    {
        public void Configure(EntityTypeBuilder<ImageDto> builder)
        {
            builder.ToTable("Image");

            builder.HasOne(t => t.Store)
                .WithMany(t => t.Images);

            builder.Property(t => t.Id)
                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
                .ValueGeneratedOnAdd();
            builder.Property(t => t.Path).IsRequired();
        }
    }
}