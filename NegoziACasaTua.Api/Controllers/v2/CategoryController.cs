using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/v2/[Controller]")]
    public class CategoryController : BaseController
    {
        private readonly ICategoryOrchestrator _categoryOrchestrator;
        private readonly IStoreReadService _storeReadService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="categoryOrchestrator"></param>
        /// <param name="storeReadService"></param>
        public CategoryController(ILogger<CategoryController> logger, ICategoryOrchestrator categoryOrchestrator, IStoreReadService storeReadService) : base(logger)
        {
            _categoryOrchestrator = categoryOrchestrator;
            _storeReadService = storeReadService;
        }
        
        
        /// <summary>
        /// Get all categories.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryJson>>> GetAllCategories()
        {
            try
            {
                var categories = await _categoryOrchestrator.GetAllCategories();
                return this.Ok(categories);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        
        /// <summary>
        /// NOTE: It will be deleted soon (it has been moved to store/admin)! Get all stores by category ordered by name.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{categoryId}")]
        public async Task<ActionResult<CategoryWithStoreJson>> GetStoresByCategoryId([FromRoute] int categoryId)
        {
            try
            {
                var stores = await _storeReadService.GetStoresByCategoryId(categoryId);
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}