using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Models;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.MailTemplateData;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.Types;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.Services.Orchestrators
{
    public class AccountOrchestrator : IAccountOrchestrator
    {
        private readonly ILogger<AccountOrchestrator> _logger;
        private readonly IAccountService _accountService;
        private readonly IStoreService _storeService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IStorageService _storageService;
        private readonly IMailService _mailService;

        public AccountOrchestrator(ILogger<AccountOrchestrator> logger, IAccountService accountService, IStoreService storeService, IUnitOfWork unitOfWork, IStorageService storageService, IMailService mailService)
        {
            _logger = logger;
            _accountService = accountService;
            _storeService = storeService;
            _unitOfWork = unitOfWork;
            _storageService = storageService;
            _mailService = mailService;
        }

        public async Task<AccountInfoJson> Authenticate(AccountCredentialsJson accountCredentialsJson)
        {
            return await _accountService.AuthenticateAsync(accountCredentialsJson);
        }

        public async Task<IEnumerable<AccountJson>> GetAllAccounts()
        {
            return await _accountService.GetAllAccountsAsync();
        }

        public async Task<AccountJson> GetAccountById(int accountId)
        {
            return await _accountService.GetAccountByIdAsync(accountId);
        }

        public async Task CreateAccount(AccountCredentialsJson accountJson, int accountType)
        {
            await _accountService.CreateAccountAsync(accountJson, accountType);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAccount(AccountJson accountJson)
        {
            await _accountService.UpdateAccountAsync(accountJson);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdatePassword(int accountId, PasswordJson password)
        {
            await _accountService.UpdatePasswordAsync(accountId, password);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAccountById(int accountId)
        {
            await _accountService.DeleteAccountAsync(accountId);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<TypeJson>> GetAllAccountTypes()
        {
            return await _accountService.GetAllAccountTypes();
        }

        public async Task<IEnumerable<AccountTypeJson>> GetAllAccountTypesWithAccounts()
        {
            return await _accountService.GetAllAccountTypesWithAccounts();
        }

        public async Task<AccountInfoJson> CreateBusiness(CreateBusinessJson businessJson)
        {
            try
            {
                await _unitOfWork.BeginTransactionAsync();
                _storageService.BeginTransaction();
                
                var accountId = await _accountService.CreateAccountAsync(businessJson.Account, 3);
                await _storeService.CreateStore(businessJson.Store, accountId);
                await _unitOfWork.SaveAsync();
                var accountInfo = await _accountService.AuthenticateAsync(businessJson.Account);

                await _storageService.CommitAsync();
                await _unitOfWork.CommitAsync();
                return accountInfo;
            }
            catch (Exception e)
            {
                await _storageService.RollbackAsync();
                await _unitOfWork.RollbackAsync();
                throw;
            }
        }

        public async Task CreateBusinessByAdmin(CreateBusinessJson businessJson)
        {
            try
            {
                await _unitOfWork.BeginTransactionAsync();
                
                businessJson.Account.Password = Password.GenerateRandom();
                var accountId = await _accountService.CreateAccountAsync(businessJson.Account, 
                    ApiAccountTypes.Business);
                await _storeService.CreateStore(businessJson.Store, accountId, true);
                
                await _mailService.SendStoreCreatedByAdminMail(businessJson.Account.Email, new StoreCreatedByAdminData
                {
                    StoreName = businessJson.Store.Name,
                    Email = businessJson.Account.Email,
                    Password = businessJson.Account.Password
                });
                
                await _unitOfWork.CommitAsync();
            }
            catch (Exception e)
            {
                await _unitOfWork.RollbackAsync();
                throw;
            }
        }
    }
}