using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/v2/[Controller]")]
    public class InitController : BaseController
    {
        private readonly ILogger<InitController> _logger;
        private readonly IInitOrchestrator _initOrchestrator;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="initOrchestrator"></param>
        public InitController(ILogger<InitController> logger, IInitOrchestrator initOrchestrator) : base(logger)
        {
            this._logger = logger;
            _initOrchestrator = initOrchestrator;
        }
        
        /// <summary>
        /// Generate default values if the database is empty.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Init()
        {
            try
            {
                await _initOrchestrator.Init();
                return this.Ok();
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}