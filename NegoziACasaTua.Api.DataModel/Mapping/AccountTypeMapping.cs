using NegoziACasaTua.Api.DataModel.Dtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NegoziACasaTua.Api.DataModel.Mapping
{
    public class AccountTypeMapping : IEntityTypeConfiguration<AccountTypeDto>
    {
        public void Configure(EntityTypeBuilder<AccountTypeDto> builder)
        {
            builder.ToTable("AccountType");
            
            builder.HasMany(t => t.Accounts)
                .WithOne(t => t.AccountType);

            builder.Property(t => t.Id);
//                .HasAnnotation("MySql:ValueGeneratedOnAdd", true)
//                .ValueGeneratedOnAdd();
            builder.Property(t => t.Name).HasMaxLength(32);
        }
    }
}