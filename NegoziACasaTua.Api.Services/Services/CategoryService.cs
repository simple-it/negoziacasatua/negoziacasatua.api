using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Factories;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.Models;

namespace NegoziACasaTua.Api.Services.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSettings _settings;

        public CategoryService(IUnitOfWork unitOfWork, IOptions<AppSettings> settings)
        {
            _unitOfWork = unitOfWork;
            _settings = settings.Value;
        }

        public async Task<IEnumerable<CategoryJson>> GetAllCategoriesAsync()
        {
            var categoriesDto = await _unitOfWork.CategoryRepository.GetQueryable()
                .OrderBy(t => t.Name).ToListAsync();
            return categoriesDto.Select(t => t.ToJson(_settings.Storage.StorageBasePath));
        }
    }
}