
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class AccountDto : DtoRoot
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool MailConfirmed { get; set; }
        
        public AccountTypeDto AccountType { get; set; }
        public StoreDto Store { get; set; }
    }
}