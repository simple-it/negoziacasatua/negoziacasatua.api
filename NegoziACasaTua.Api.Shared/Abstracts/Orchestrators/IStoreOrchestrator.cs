using System.Collections.Generic;
using System.Threading.Tasks;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Shared.Abstracts.Orchestrators
{
    public interface IStoreOrchestrator
    {
        Task EditOwnerEmail(int storeId, EmailJson emailJson);
        Task EditStoreById(int storeId, StoreToEditJson storeJson, bool validated = false);
        Task ValidateStoreById(int id);
        Task MarkStoreAsDeleted(int id);
    }
}