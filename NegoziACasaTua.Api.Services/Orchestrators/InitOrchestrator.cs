using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Dtos;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NegoziACasaTua.Api.Shared.Configuration;
using NegoziACasaTua.Api.Shared.MailTemplateData;
using NegoziACasaTua.Api.Shared.Types;
using NegoziACasaTua.Api.Shared.ValueObjects;

namespace NegoziACasaTua.Api.Services.Orchestrators
{
    public class InitOrchestrator : IInitOrchestrator
    {
        private readonly AppSettings _settings;
        private readonly IAccountService _accountService;
        private readonly IMailService _mailService;
        private readonly IUnitOfWork _unitOfWork;

        public InitOrchestrator(IAccountService accountService, IUnitOfWork unitOfWork, IOptions<AppSettings> settings, IMailService mailService)
        {
            _accountService = accountService;
            _unitOfWork = unitOfWork;
            _mailService = mailService;
            _settings = settings.Value;
        }
        
        public async Task Init()
        {
            //await CreateStoreDefaultAccounts();
            await CheckAccountTypes();
            await CheckAccounts();
            await CheckContactTypes();
            await CheckCategories();
            await CheckPaymentTypes();
        }




        private async Task CreateStoreDefaultAccounts()
        {
            var accountsDto = await _unitOfWork.AccountRepository.GetQueryable().Include(t => t.Store)
                .Where(t => string.IsNullOrEmpty(t.Password))
                .ToListAsync();

            var accountsWithoutPassword = new List<string>();
            var emailSent = new List<string>();

            var sendMailTasks = accountsDto.Select(account =>
            {
                if (account.Store == null)
                {
                    accountsWithoutPassword.Add(account.Email);
                    return Task.CompletedTask;
                }
                if (account.Store.DeletedDate != null)
                    return Task.CompletedTask;

                var password = Password.GenerateRandom();
                account.Password = new Password(password).GetValue();
                _unitOfWork.AccountRepository.Update(account);
                
                emailSent.Add(account.Email);

                return _mailService.SendStoreCreatedByAdminMail(account.Email, new StoreCreatedByAdminData
                {
                    StoreName = account.Store.Name,
                    Email = account.Email,
                    Password = password
                });
            });

            await Task.WhenAll(sendMailTasks);
            await _unitOfWork.SaveAsync();

            var emailText = "Per visualizzare la mail occorre attivare l'html";
            var emailHtml = "<p>Ecco il riepilogo degli account generati in automatico:</p>";
            emailHtml += "<ul>";
            foreach (var email in emailSent)
            {
                emailHtml += $"<li>{email}</li>";
            }
            emailHtml += "</ul>";
            if (accountsWithoutPassword.Any())
            {
                emailHtml += "<p>Qui sotto invece ci sono gli account senza password e senza negozio che ci sono sul sito (serve a noi sviluppatori):</p>";
                emailHtml += "<ul>";
                foreach (var email in accountsWithoutPassword)
                {
                    emailHtml += $"<li>{email}</li>";
                }
                emailHtml += "</ul>";
            }

            await _mailService.SendMail("ales.zanola@gmail.com", "Alessandro Zanola",
                "Riepilogo account creati", emailText, emailHtml);
            
            await _mailService.SendMail("marilena.mura@comune.desenzano.brescia.it", "Marilena Mura",
                "Riepilogo account creati", emailText, emailHtml);
        }
        

        private async Task CheckAccountTypes()
        {
            var count = await _unitOfWork.AccountTypeRepository.GetQueryable().CountAsync();
            if (count != 0) 
                return;

            await _unitOfWork.AccountTypeRepository.Insert(new AccountTypeDto
            {
                Id = ApiAccountTypes.Admin,
                Name = "Admin"
            });
            await _unitOfWork.AccountTypeRepository.Insert(new AccountTypeDto
            {
                Id = ApiAccountTypes.Municipality,
                Name = "Amministrazione comune"
            });
            await _unitOfWork.AccountTypeRepository.Insert(new AccountTypeDto
            {
                Id = ApiAccountTypes.Business,
                Name = "Business"
            });
            
            await _unitOfWork.SaveAsync();
        }

        private async Task CheckAccounts()
        {
            var accountsCount = await _unitOfWork.AccountRepository.GetQueryable().CountAsync();
            if (accountsCount != 0)
                return;
            
            await _accountService.CreateAccountAsync(new AccountCredentialsJson
            {
                Email = "ales.zanola@gmail.com",
                Password = "admin"
            }, ApiAccountTypes.Admin);
        }

        private async Task CheckContactTypes()
        {
            var contactTypesCount = await _unitOfWork.ContactTypeRepository.GetQueryable().CountAsync();
            if(contactTypesCount  != 0)
                return;

            await _unitOfWork.ContactTypeRepository.Insert(new ContactTypeDto
            {
                Id = ApiContactTypes.Email,
                Name = "Email",
                BaseUrlPath = "mailto:"
            });
            await _unitOfWork.ContactTypeRepository.Insert(new ContactTypeDto
            {
                Id = ApiContactTypes.WhatsApp,
                Name = "WhatsApp",
                BaseUrlPath = "https://wa.me/39"
            });
            await _unitOfWork.ContactTypeRepository.Insert(new ContactTypeDto
            {
                Id = ApiContactTypes.Telephone,
                Name = "Telefono",
                BaseUrlPath = "tel:"
            });
            
            await _unitOfWork.SaveAsync();
        }

        private async Task CheckCategories()
        {
            var count = await _unitOfWork.CategoryRepository.GetQueryable().CountAsync();
            if(count != 0)
                return;

            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Alimentari",
                ImagePath = _settings.Storage.BasePathAppImages + "alimentari.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Bevande",
                ImagePath = _settings.Storage.BasePathAppImages + "bevande.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Casa",
                ImagePath = _settings.Storage.BasePathAppImages + "casa.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Elettronica e informatica",
                ImagePath = _settings.Storage.BasePathAppImages + "informatica.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Libri e giornali",
                ImagePath = _settings.Storage.BasePathAppImages + "libri_giornali.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Ristorazione",
                ImagePath = _settings.Storage.BasePathAppImages + "ristorazione.jpg"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.CategoryRepository.Insert(new CategoryDto
            {
                Name = "Salute e persona",
                ImagePath = _settings.Storage.BasePathAppImages + "salute.jpg"
            });
            await _unitOfWork.SaveAsync();
        }

        public async Task CheckPaymentTypes()
        {
            var count = await _unitOfWork.PaymentTypeRepository.GetQueryable().CountAsync();
            if(count != 0)
                return;

            await _unitOfWork.PaymentTypeRepository.Insert(new PaymentTypeDto
            {
                Name = "In contanti alla consegna"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.PaymentTypeRepository.Insert(new PaymentTypeDto
            {
                Name = "Bancomat o Carta di Credito"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.PaymentTypeRepository.Insert(new PaymentTypeDto
            {
                Name = "Bonifico bancario"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.PaymentTypeRepository.Insert(new PaymentTypeDto
            {
                Name = "Satispay"
            });
            await _unitOfWork.SaveAsync();
            await _unitOfWork.PaymentTypeRepository.Insert(new PaymentTypeDto
            {
                Name = "Bancomat online"
            });
            await _unitOfWork.SaveAsync();
        }
    }
}