using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.Shared.Abstracts.ReadModelServices;
using NegoziACasaTua.Api.Shared.Models.v2;
using NegoziACasaTua.Api.Shared.Types;

namespace NegoziACasaTua.Api.Controllers.v2
{
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/Store/Admin")]
    public class StoreAdminReadController : BaseController
    {
        private readonly IStoreReadService _storeReadService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="storeReadService"></param>
        public StoreAdminReadController(ILogger<StoreAdminReadController> logger, IStoreReadService storeReadService) : base(logger)
        {
            _storeReadService = storeReadService;
        }
        
        
        /// <summary>
        /// Require login (admin or municipality). Get all stores (validated).
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StoreAdminJson>>> GetAllStoresValidated()
        {
            try
            {
                var stores = await _storeReadService.GetAllStores();
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin or municipality). Get all the stores (to validated).
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpGet("tovalidate")]
        public async Task<ActionResult<IEnumerable<StoreAdminJson>>> GetAllStoresToValidate()
        {
            try
            {
                var stores = await _storeReadService.GetAllStores(false);
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (admin or municipality). Get the number of store to validate.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "1,2")]
        [HttpGet("count")]
        public async Task<ActionResult<ValueJson>> GetStoresToValidateCount()
        {
            try
            {
                var stores = await _storeReadService.GetStoresToValidateCount();
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
        
        /// <summary>
        /// Require login (business). Get info about a store.
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "3")]
        [HttpGet("info")]
        public async Task<ActionResult<StoreInfoJson>> GetStoreInfo()
        {
            try
            {
                var stores = await _storeReadService.GetStoreInfo(int.Parse(GetClaim(ApiClaimTypes.StoreId)));
                return this.Ok(stores);
            }
            catch (Exception ex)
            {
                return AnswerWithError(ex);
            }
        }
    }
}