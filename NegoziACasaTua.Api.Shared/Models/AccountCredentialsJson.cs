namespace NegoziACasaTua.Api.Shared.Models
{
    public class AccountCredentialsJson
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}