using System.Collections.Generic;
using NegoziACasaTua.Api.DataModel.Abstracts;

namespace NegoziACasaTua.Api.DataModel.Dtos
{
    public class PaymentTypeDto : DtoRoot
    {
        public string Name { get; set; }
        
        public ICollection<PaymentDto> Payments { get; set; }
    }
}