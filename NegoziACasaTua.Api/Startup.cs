using System;
using System.IO;
using System.Text;
using NegoziACasaTua.Api.DataModel.Facade;
using NegoziACasaTua.Api.Mediator;
using NegoziACasaTua.Api.Shared.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace NegoziACasaTua.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", corsBuilder =>
                    corsBuilder.WithOrigins("https://negoziacasatua.web.app",
                                            "https://negoziacasatua.firebaseapp.com",
                                            "http://localhost:8080")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });
            #endregion

            #region AppSettings

            var appSettingsSection = this.Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();
            
            #endregion

            #region Dependency injection and DB

            services.AddControllers();
            services.AddMySqlServer();
            services.AddServices();
            services.AddReadServices();
            services.AddOrchestrators();
            
            services.AddDbContext<DataModelFacade>(options =>
            {
                options.UseMySql(appSettings.MySqlConnectionString);
            });

            #endregion

            #region Authentication

            var key = Encoding.ASCII.GetBytes(appSettings.TokenAuthentication.SecretKey);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                    x.Events = new JwtBearerEvents()
                    {
                        OnChallenge = c =>
                        {
                            c.HandleResponse();
                            
                            c.Response.StatusCode = 401;
                            c.Response.ContentType = "application/json";
                     
                            var r = new JObject
                            {
                                ["error"] = c.Error,
                                ["errorDescription"] = c.ErrorDescription
                            };
                            
                            return c.Response.WriteAsync(r.ToString());
                        }
                    };
                });

            #endregion

            #region Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v2", new OpenApiInfo
                {
                    Title = "Negozi a casa tua Project API V2",
                    Version = "v2",
                    Description = "Web Api Services of Simple, a group of students",
                    Contact = new OpenApiContact()
                    {
                        Name = "Alessandro Zanola",
                        Email = "ales.zanola@gmail.com"
                    }
                });
                
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    BearerFormat = "JWT"
                });

                var basePath = AppContext.BaseDirectory;
                var assemblyName = System.Reflection.Assembly.GetEntryAssembly()?.GetName().Name;
                var fileName = Path.GetFileName(assemblyName + ".xml");
                var filePath = Path.Combine(basePath, fileName);
                if (File.Exists(filePath))
                    c.IncludeXmlComments(filePath);
            });
            #endregion
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="applicationLifetime"></param>
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime applicationLifetime)
        {
            #region Logging
            loggerFactory.AddSerilog();
            loggerFactory.AddFile("Logs/negoziacasatua-{Date}.txt");

            // Ensure any buffered events are sent at shutdown
            applicationLifetime.ApplicationStopped.Register(Log.CloseAndFlush);
            #endregion

            #region Routing
            app.UseRouting();
            #endregion

            #region CORS
            app.UseCors("CorsPolicy");
            #endregion
            
            #region Authentication
            app.UseAuthentication();
            app.UseAuthorization();
            #endregion

            #region Endpoints routing
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            #endregion

            #region Swagger
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "documentation/{documentName}/documentation.json";
            });
            app.UseSwaggerUI(c =>
            {
                //c.ShowRequestHeaders();
                c.SwaggerEndpoint("/documentation/v2/documentation.json", "NegoziACasaTua Web API V2");
            });
            #endregion
        }
    }
}