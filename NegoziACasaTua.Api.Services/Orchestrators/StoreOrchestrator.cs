using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Apis.Logging;
using Microsoft.Extensions.Logging;
using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.Services.Utilities;
using NegoziACasaTua.Api.Shared.Abstracts.Orchestrators;
using NegoziACasaTua.Api.Shared.Abstracts.Services;
using NegoziACasaTua.Api.Shared.Models;
using NegoziACasaTua.Api.Shared.Models.v2;

namespace NegoziACasaTua.Api.Services.Orchestrators
{
    public class StoreOrchestrator : IStoreOrchestrator
    {
        private readonly ILogger<StoreOrchestrator> _logger;
        private readonly IStoreService _storeService;
        private readonly IStorageService _storageService;
        private readonly IUnitOfWork _unitOfWork;

        public StoreOrchestrator(IStoreService storeService, IUnitOfWork unitOfWork, IStorageService storageService, ILogger<StoreOrchestrator> logger)
        {
            _storeService = storeService;
            _unitOfWork = unitOfWork;
            _storageService = storageService;
            _logger = logger;
        }

        public async Task EditOwnerEmail(int storeId, EmailJson emailJson)
        {
            await _storeService.EditOwnerEmail(storeId, emailJson);
            await _unitOfWork.SaveAsync();
        }

        public async Task EditStoreById(int storeId, StoreToEditJson storeJson, bool validated = false)
        {
            try
            {
                _storageService.BeginTransaction();
                await _unitOfWork.BeginTransactionAsync();

                await _storeService.EditStore(storeId, storeJson, validated);

                await _storageService.CommitAsync();
                await _unitOfWork.CommitAsync();
            }
            catch (Exception e)
            {
                await _storageService.RollbackAsync();
                await _unitOfWork.RollbackAsync();
                throw;
            }
        }

        public async Task ValidateStoreById(int id)
        {
            await _storeService.ValidateStoreById(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task MarkStoreAsDeleted(int id)
        {
            try
            {
                await _unitOfWork.BeginTransactionAsync();
                await _storeService.MarkStoreAsDeleted(id);
                await _unitOfWork.CommitAsync();
            }
            catch (Exception e)
            {
                CommonServices.LogDefaultExceptionErrors(e, _logger, this);
                throw;
            }
        }
    }
}