﻿using System.Linq;
using System.Threading.Tasks;
using NegoziACasaTua.Api.DataModel.Dtos;

namespace NegoziACasaTua.Api.DataModel.Abstracts
{
    public interface IUnitOfWork
    {
        IRepository<AccountDto> AccountRepository { get; }
        IRepository<AccountTypeDto> AccountTypeRepository { get; }
        IRepository<CategoryDto> CategoryRepository { get; }
        IRepository<ContactDto> ContactRepository { get; }
        IRepository<ContactTypeDto> ContactTypeRepository { get; }
        IRepository<ImageDto> ImageRepository { get; }
        IRepository<PaymentDto> PaymentRepository { get; }
        IRepository<PaymentTypeDto> PaymentTypeRepository { get; }
        IRepository<StoreDto> StoreRepository { get; }
        
        
        Task BeginTransactionAsync();
        Task CommitAsync();
        Task RollbackAsync();
        Task SaveAsync();
    }
}