using System.Collections.Generic;

namespace NegoziACasaTua.Api.Shared.Models
{
    public class AccountTypeJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<AccountNoPasswordJson> Accounts { get; set; }
    }
}