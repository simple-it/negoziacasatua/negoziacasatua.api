namespace NegoziACasaTua.Api.Shared.Models.v2
{
    public class StoreAdminJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string OwnerEmail { get; set; }
    }
}