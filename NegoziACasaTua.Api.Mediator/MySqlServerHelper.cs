using NegoziACasaTua.Api.DataModel.Abstracts;
using NegoziACasaTua.Api.DataModel.Facade;
using NegoziACasaTua.Api.DataModel.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace NegoziACasaTua.Api.Mediator
{
    public static class MySqlServerHelper
    {
        public static IServiceCollection AddMySqlServer(this IServiceCollection services)
        {
            services.AddScoped<DataModelFacade>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
            return services;
        }
    }
}