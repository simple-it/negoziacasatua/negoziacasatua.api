using System.Collections.Generic;
using System.Threading.Tasks;

namespace NegoziACasaTua.Api.Shared.Abstracts.Services
{
    public interface IImageService
    {
        Task<string> UploadImageAndGetPathAsync(string base64, int imagesNumber = 0);
        Task DeleteIfExists(string path);
        Task DeleteIfExists(IEnumerable<string> paths);
    }
}